﻿CREATE TABLE [dbo].[KasperskyConfigPackage] (
    [KasperskyConfigPackageId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (100) NULL,
    [Status]                   INT            NULL,
    [LastUpdated]              DATETIME       NULL,
    [DateCreated]              DATETIME       NULL,
    [Physical]                 INT            NULL,
    CONSTRAINT [PK_KasperskyConfigPackage] PRIMARY KEY CLUSTERED ([KasperskyConfigPackageId] ASC)
);

