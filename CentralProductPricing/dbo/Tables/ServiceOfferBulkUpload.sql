﻿CREATE TABLE [dbo].[ServiceOfferBulkUpload] (
    [ServiceOfferBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                  INT            NOT NULL,
    [UserAccountId]            INT            NULL,
    [Name]                     NVARCHAR (50)  NULL,
    [FileName]                 NVARCHAR (MAX) NULL,
    [Status]                   INT            NOT NULL,
    [DateCreated]              DATETIME       NULL,
    [LastUpdated]              DATETIME       NULL,
    [ErrorRows]                VARCHAR (MAX)  NULL,
    [Override]                 BIT            NOT NULL,
    [OverrideData]             BIT            NULL,
    [DateStarted]              DATETIME       NULL,
    CONSTRAINT [PK_ServiceOfferBulkUpload] PRIMARY KEY CLUSTERED ([ServiceOfferBulkUploadId] ASC),
    CONSTRAINT [FK_ServiceOfferBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_ServiceOfferBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);







