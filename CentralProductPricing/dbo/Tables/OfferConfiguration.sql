﻿CREATE TABLE [dbo].[OfferConfiguration] (
    [OfferConfigurationId] INT IDENTITY (1, 1) NOT NULL,
    [CPPOfferId]           INT NULL,
    [IsRemoved]            BIT NULL,
    [IsCreateEmailSent]    BIT NULL,
    CONSTRAINT [PK_OfferConfiguration] PRIMARY KEY CLUSTERED ([OfferConfigurationId] ASC)
);

