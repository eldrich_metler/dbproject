﻿CREATE TABLE [dbo].[VeeamProductHistory] (
    [VeeamProductHistoryId]                 INT             IDENTITY (1, 1) NOT NULL,
    [DateCreated]                           DATETIME        NULL,
    [LastUpdated]                           DATETIME        NULL,
    [UserAccountId]                         INT             NULL,
    [VeeamProductId]                        INT             NULL,
    [DistiListPrice]                        DECIMAL (12, 2) NULL,
    [DistiDiscount]                         DECIMAL (5, 2)  NULL,
    [Discount]                              DECIMAL (5, 2)  NULL,
    [DistiPurchasePrice]                    DECIMAL (12, 2) NULL,
    [DistiWithDealRegSixPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTenPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTwelvePercentDiscount] DECIMAL (12, 2) NULL,
    CONSTRAINT [PK_VeeamProductHistory] PRIMARY KEY CLUSTERED ([VeeamProductHistoryId] ASC),
    CONSTRAINT [FK_VeeamProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    CONSTRAINT [FK_VeeamProductHistory_VeeamProduct] FOREIGN KEY ([VeeamProductId]) REFERENCES [dbo].[VeeamProduct] ([VeeamProductId])
);

