﻿CREATE TABLE [dbo].[ServiceOfferBulkUploadItem] (
    [ServiceOfferBulkUploadItemId] INT             IDENTITY (1, 1) NOT NULL,
    [ServiceOfferBulkUploadId]     INT             NOT NULL,
    [Name]                         NVARCHAR (150)  NOT NULL,
    [Description]                  NVARCHAR (500)  NULL,
    [MinimumQuantity]              INT             NULL,
    [IsProRate]                    BIT             NOT NULL,
    [PricingBillingCycle]          NVARCHAR (50)   NOT NULL,
    [VendorSKU]                    NVARCHAR (100)  NOT NULL,
    [DistributorSKU]               NVARCHAR (100)  NULL,
    [UnitOfMeasure]                NVARCHAR (100)  NOT NULL,
    [Usage]                        INT             NULL,
    [ItemOrder]                    INT             NULL,
    [Source]                       INT             NULL,
    [VendorPrice]                  DECIMAL (18, 2) NULL,
    [ResellerPrice]                DECIMAL (18, 2) NULL,
    [ResellerMargin]               DECIMAL (18, 2) NULL,
    [Status]                       INT             NOT NULL,
    [DateCreated]                  DATETIME        NOT NULL,
    [DateModified]                 DATETIME        NOT NULL,
    [DateArchived]                 DATETIME        NULL,
    CONSTRAINT [PK_ServiceOfferBulkUploadItem] PRIMARY KEY CLUSTERED ([ServiceOfferBulkUploadItemId] ASC),
    CONSTRAINT [FK_ServiceOfferBulkUploadItem_ServiceOfferBulkUpload] FOREIGN KEY ([ServiceOfferBulkUploadId]) REFERENCES [dbo].[ServiceOfferBulkUpload] ([ServiceOfferBulkUploadId])
);





