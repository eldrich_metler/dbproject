﻿CREATE TABLE [dbo].[VeritasConfigBand] (
    [VeritasConfigBandId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (50) NULL,
    [MinimumQuantity]     INT           NULL,
    [MaximumQuantity]     INT           NULL,
    [Status]              INT           NULL,
    [LastUpdated]         DATETIME      NULL,
    [DateCreated]         DATETIME      NULL,
    CONSTRAINT [PK_VeritasConfigBand] PRIMARY KEY CLUSTERED ([VeritasConfigBandId] ASC)
);

