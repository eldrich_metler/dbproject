﻿CREATE TABLE [dbo].[LGProductBulkUploadItemV1] (
    [LGProductBulkUploadItemId] INT             IDENTITY (1, 1) NOT NULL,
    [LGProductBulkUploadId]     INT             NULL,
    [LGConfigSegId]             INT             NULL,
    [LGConfigSeriesId]          INT             NULL,
    [LGConfigInchId]            INT             NULL,
    [Model]                     NVARCHAR (20)   NULL,
    [Description]               NVARCHAR (MAX)  NULL,
    [Price]                     DECIMAL (10, 2) NULL,
    [Status]                    INT             NULL,
    [LastUpdated]               DATETIME        NULL,
    [DateCreated]               DATETIME        NULL
);

