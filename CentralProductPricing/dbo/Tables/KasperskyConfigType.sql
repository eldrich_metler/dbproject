﻿CREATE TABLE [dbo].[KasperskyConfigType] (
    [KasperskyConfigTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (100) NULL,
    [Status]                INT            NULL,
    [LastUpdated]           DATETIME       CONSTRAINT [DF_KasperskyConfigType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]           DATETIME       CONSTRAINT [DF_KasperskyConfigType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_KasperskyConfigType] PRIMARY KEY CLUSTERED ([KasperskyConfigTypeId] ASC)
);

