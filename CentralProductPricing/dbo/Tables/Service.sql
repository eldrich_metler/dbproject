﻿CREATE TABLE [dbo].[Service] (
    [ServiceId]          INT            IDENTITY (1, 1) NOT NULL,
    [ServiceCode]        NVARCHAR (MAX) NULL,
    [BrandId]            INT            NOT NULL,
    [ServiceGroupId]     INT            NOT NULL,
    [Name]               NVARCHAR (250) NULL,
    [Description]        NVARCHAR (MAX) NULL,
    [MoreInformation]    NVARCHAR (MAX) NULL,
    [FAQ]                NVARCHAR (MAX) NULL,
    [SLA]                NVARCHAR (MAX) NULL,
    [SystemRequirements] NVARCHAR (MAX) NULL,
    [APIIntegration]     BIT            NULL,
    [Status]             INT            NOT NULL,
    [DateCreated]        DATETIME       NOT NULL,
    [DateModified]       DATETIME       NOT NULL,
    [DateArchived]       DATETIME       NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([ServiceId] ASC),
    CONSTRAINT [FK_Service_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_Service_ServiceGroup] FOREIGN KEY ([ServiceGroupId]) REFERENCES [dbo].[ServiceGroup] ([ServiceGroupId])
);





