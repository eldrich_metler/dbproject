﻿CREATE TABLE [dbo].[ZimbraProductHistory] (
    [ZimbraProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [UserAccountId]          INT             NULL,
    [ZimbraProductBandId]    INT             NULL,
    [DateCreated]            DATETIME        NULL,
    [LastUpdated]            DATETIME        NULL,
    [ListPrice]              DECIMAL (12, 2) NULL,
    [ExtendedPrice]          DECIMAL (12, 2) NULL,
    CONSTRAINT [PK_ZimbraProductHistory] PRIMARY KEY CLUSTERED ([ZimbraProductHistoryId] ASC),
    CONSTRAINT [FK_ZimbraProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    CONSTRAINT [FK_ZimbraProductHistory_ZimbraProductBand] FOREIGN KEY ([ZimbraProductBandId]) REFERENCES [dbo].[ZimbraProductBand] ([ZimbraProductBandId])
);

