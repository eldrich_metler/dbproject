﻿CREATE TABLE [dbo].[SamsungConfigTermsofCost] (
    [SamsungConfigTermsofCostId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                       NVARCHAR (50) NULL,
    [Status]                     INT           NULL,
    [LastUpdated]                DATETIME      CONSTRAINT [DF_SamsungConfigTermsofCost_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]                DATETIME      CONSTRAINT [DF_SamsungConfigTermsofCost_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigTermsofCost] PRIMARY KEY CLUSTERED ([SamsungConfigTermsofCostId] ASC)
);

