﻿CREATE TABLE [dbo].[KasperskyProduct] (
    [KasperskyProductId]        INT             IDENTITY (1, 1) NOT NULL,
    [BrandId]                   INT             NULL,
    [Name]                      NVARCHAR (200)  NULL,
    [Description]               NVARCHAR (MAX)  NULL,
    [KasperskyConfigCategoryId] INT             NULL,
    [KasperskyConfigPackageId]  INT             NULL,
    [KasperskyConfigTypeId]     INT             NULL,
    [KasperskyConfigTermId]     INT             NULL,
    [KasperskyConfigVolumeId]   INT             NULL,
    [KasperskyConfigBandId]     INT             NULL,
    [KasperskyConfigLanguageId] INT             NULL,
    [Price]                     DECIMAL (10, 2) NULL,
    [SKU]                       VARCHAR (50)    NULL,
    [Status]                    INT             NULL,
    [DateCreated]               DATETIME        NULL,
    [LastUpdated]               DATETIME        NULL,
    CONSTRAINT [PK_KasperskyProduct] PRIMARY KEY CLUSTERED ([KasperskyProductId] ASC),
    CONSTRAINT [FK_KasperskyProduct_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigBand] FOREIGN KEY ([KasperskyConfigBandId]) REFERENCES [dbo].[KasperskyConfigBand] ([KasperskyConfigBandId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigCategory] FOREIGN KEY ([KasperskyConfigCategoryId]) REFERENCES [dbo].[KasperskyConfigCategory] ([KasperskyConfigCategoryId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigLanguage] FOREIGN KEY ([KasperskyConfigLanguageId]) REFERENCES [dbo].[KasperskyConfigLanguage] ([KasperskyConfigLanguageId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigPackage] FOREIGN KEY ([KasperskyConfigPackageId]) REFERENCES [dbo].[KasperskyConfigPackage] ([KasperskyConfigPackageId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigTerm] FOREIGN KEY ([KasperskyConfigTermId]) REFERENCES [dbo].[KasperskyConfigTerm] ([KasperskyConfigTermId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigType] FOREIGN KEY ([KasperskyConfigTypeId]) REFERENCES [dbo].[KasperskyConfigType] ([KasperskyConfigTypeId]),
    CONSTRAINT [FK_KasperskyProduct_KasperskyConfigVolume] FOREIGN KEY ([KasperskyConfigVolumeId]) REFERENCES [dbo].[KasperskyConfigVolume] ([KasperskyConfigVolumeId])
);



