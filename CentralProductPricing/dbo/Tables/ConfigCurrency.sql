﻿CREATE TABLE [dbo].[ConfigCurrency] (
    [ConfigCurrencyId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (100) NULL,
    [Symbol]           NVARCHAR (5)   NULL,
    [Status]           INT            NULL,
    [LastUpdated]      DATETIME       NULL,
    [DateCreated]      DATETIME       NOT NULL,
    [ShortName]        VARCHAR (3)    NULL,
    CONSTRAINT [PK_ConfigCurrency] PRIMARY KEY CLUSTERED ([ConfigCurrencyId] ASC)
);

