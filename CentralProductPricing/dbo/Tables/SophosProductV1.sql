﻿CREATE TABLE [dbo].[SophosProductV1] (
    [SophosProductId]                INT             IDENTITY (1, 1) NOT NULL,
    [BrandId]                        INT             NULL,
    [SophosConfigCategoryId]         INT             NULL,
    [SophosConfigProductTypeId]      INT             NULL,
    [SophosConfigDiscountCategoryId] INT             NULL,
    [Name]                           NVARCHAR (200)  NULL,
    [Description]                    NVARCHAR (MAX)  NULL,
    [Status]                         INT             NULL,
    [DateCreated]                    DATETIME        NULL,
    [LastUpdated]                    DATETIME        NULL,
    [SophosConfigSectorId]           INT             NULL,
    [SophosConfigTermId]             INT             NULL,
    [SophosConfigDealTypeId]         INT             NULL,
    [Units]                          VARCHAR (100)   NULL,
    [MinimumQuantity]                INT             NULL,
    [MaximumQuantity]                INT             NULL,
    [Term]                           INT             NULL,
    [Sector]                         VARCHAR (100)   NULL,
    [ProductFamily]                  VARCHAR (100)   NULL,
    [ProductType]                    VARCHAR (100)   NULL,
    [DealType]                       VARCHAR (100)   NULL,
    [SKU]                            VARCHAR (10)    NULL,
    [Price]                          DECIMAL (12, 2) NULL
);



