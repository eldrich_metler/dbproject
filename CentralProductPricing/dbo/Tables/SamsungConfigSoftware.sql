﻿CREATE TABLE [dbo].[SamsungConfigSoftware] (
    [SamsungConfigSoftwareId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (100) NULL,
    [Status]                  INT            NULL,
    [LastUpdated]             DATETIME       CONSTRAINT [DF_SamsungConfigSoftware_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]             DATETIME       CONSTRAINT [DF_SamsungConfigSoftware_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigSoftware] PRIMARY KEY CLUSTERED ([SamsungConfigSoftwareId] ASC)
);

