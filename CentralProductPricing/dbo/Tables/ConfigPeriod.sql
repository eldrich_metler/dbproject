﻿CREATE TABLE [dbo].[ConfigPeriod] (
    [ConfigPeriodId] INT          IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (50) NOT NULL,
    [Status]         INT          NULL,
    [DateCreated]    DATETIME     NULL,
    [LastUpdated]    DATETIME     NULL,
    CONSTRAINT [PK_ConfigPeriod] PRIMARY KEY CLUSTERED ([ConfigPeriodId] ASC)
);

