﻿CREATE TABLE [dbo].[MicrosoftCSPOfferHistory] (
    [MicrosoftCSPOfferHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [MicrosoftCSPOfferId]        INT             NULL,
    [ListPrice]                  DECIMAL (12, 2) NULL,
    [ERPPrice]                   DECIMAL (12, 2) NULL,
    [DateCreated]                DATETIME        NULL,
    [LastUpdated]                DATETIME        NULL,
    [UserAccountId]              INT             NULL,
    CONSTRAINT [PK_MicrosoftCSPOfferHistory] PRIMARY KEY CLUSTERED ([MicrosoftCSPOfferHistoryId] ASC),
    CONSTRAINT [FK_MicrosoftCSPOfferHistory_MicrosoftCSPOffer] FOREIGN KEY ([MicrosoftCSPOfferId]) REFERENCES [dbo].[MicrosoftCSPOffer] ([MicrosoftCSPOfferId])
);

