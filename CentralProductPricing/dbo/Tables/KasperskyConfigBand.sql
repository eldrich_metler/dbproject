﻿CREATE TABLE [dbo].[KasperskyConfigBand] (
    [KasperskyConfigBandId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (50) NULL,
    [MinimumQuantity]       INT           NULL,
    [MaximumQuantity]       INT           NULL,
    [Status]                INT           NULL,
    [LastUpdated]           DATETIME      NULL,
    [DateCreated]           DATETIME      NOT NULL,
    CONSTRAINT [PK_KasperskyConfigBands] PRIMARY KEY CLUSTERED ([KasperskyConfigBandId] ASC)
);

