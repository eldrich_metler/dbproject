﻿CREATE TABLE [dbo].[DomainsProductTLDApplicationExcluded] (
    [DomainsProductTLDApplicationExcludedId] INT IDENTITY (1, 1) NOT NULL,
    [DomainsProductTLDId]                    INT NULL,
    [ApplicationId]                          INT NULL,
    PRIMARY KEY CLUSTERED ([DomainsProductTLDApplicationExcludedId] ASC),
    CONSTRAINT [FK_DomainsProductTLDApplicationExcluded_Application] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Application] ([ApplicationId]),
    CONSTRAINT [FK_DomainsProductTLDApplicationExcluded_DomainsProductTLD] FOREIGN KEY ([DomainsProductTLDId]) REFERENCES [dbo].[DomainsProductTLD] ([DomainsProductTLDId])
);

