﻿CREATE TABLE [dbo].[ZimbraLicenseType] (
    [ZimbraLicenseTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (100) NULL,
    [DateCreated]         DATETIME       NULL,
    [LastUpdated]         DATETIME       NULL,
    [Status]              INT            NULL,
    CONSTRAINT [PK_ZimbraLicenseType] PRIMARY KEY CLUSTERED ([ZimbraLicenseTypeId] ASC)
);

