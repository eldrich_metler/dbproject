﻿CREATE TABLE [dbo].[DomainsProductApplication] (
    [DomainsProductApplicationId] INT IDENTITY (1, 1) NOT NULL,
    [DomainsProductId]            INT NULL,
    [ApplicationId]               INT NULL,
    PRIMARY KEY CLUSTERED ([DomainsProductApplicationId] ASC),
    CONSTRAINT [FK_DomainsProductApplication_Appilcation] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Application] ([ApplicationId]),
    CONSTRAINT [FK_DomainsProductApplication_DomainsProduct] FOREIGN KEY ([DomainsProductId]) REFERENCES [dbo].[DomainsProduct] ([DomainsProductId])
);

