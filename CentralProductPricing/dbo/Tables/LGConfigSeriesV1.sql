﻿CREATE TABLE [dbo].[LGConfigSeriesV1] (
    [LGConfigSeriesId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (20) NULL,
    [Status]           INT           NULL,
    [LastUpdated]      DATETIME      CONSTRAINT [DF_LGConfigSeries_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]      DATETIME      CONSTRAINT [DF_LGConfigSeries_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_LGConfigSeries] PRIMARY KEY CLUSTERED ([LGConfigSeriesId] ASC)
);

