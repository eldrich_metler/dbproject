﻿CREATE TABLE [dbo].[SophosProductBulkUpload] (
    [SophosProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                   INT            NULL,
    [UserAccountId]             INT            NULL,
    [Name]                      NVARCHAR (50)  NULL,
    [FileName]                  NVARCHAR (150) NULL,
    [Status]                    INT            NULL,
    [DateCreated]               DATETIME       NULL,
    [LastUpdated]               DATETIME       NULL,
    [ErrorRows]                 VARCHAR (MAX)  NULL,
    [Override]                  BIT            NOT NULL,
    [DateStarted]               DATETIME       NULL,
    CONSTRAINT [PK_SophosProductBulkUpload] PRIMARY KEY CLUSTERED ([SophosProductBulkUploadId] ASC),
    CONSTRAINT [FK_SophosProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_SophosProductBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

