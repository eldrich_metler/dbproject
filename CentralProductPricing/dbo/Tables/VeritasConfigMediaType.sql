﻿CREATE TABLE [dbo].[VeritasConfigMediaType] (
    [VeritasConfigMediaTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (100) NULL,
    [Status]                   INT            NULL,
    [LastUpdated]              DATETIME       CONSTRAINT [DF_VeritasConfigMediaType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]              DATETIME       CONSTRAINT [DF_VeritasConfigMediaType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigMediaType] PRIMARY KEY CLUSTERED ([VeritasConfigMediaTypeId] ASC)
);

