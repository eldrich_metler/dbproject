﻿CREATE TABLE [dbo].[VeritasConfigPlatform] (
    [VeritasConfigPlatformId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (100) NULL,
    [Status]                  INT            NULL,
    [LastUpdated]             DATETIME       CONSTRAINT [DF_VeritasConfigPlatform_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]             DATETIME       CONSTRAINT [DF_VeritasConfigPlatform_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigPlatform] PRIMARY KEY CLUSTERED ([VeritasConfigPlatformId] ASC)
);

