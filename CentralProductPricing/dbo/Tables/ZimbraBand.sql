﻿CREATE TABLE [dbo].[ZimbraBand] (
    [ZimbraBandId]    INT      IDENTITY (1, 1) NOT NULL,
    [MinimumQuantity] INT      NULL,
    [MaximumQuantity] INT      NULL,
    [DateCreated]     DATETIME NULL,
    [LastUpdated]     DATETIME NULL,
    [Status]          INT      NULL,
    CONSTRAINT [PK_ZimbraBand] PRIMARY KEY CLUSTERED ([ZimbraBandId] ASC)
);

