﻿CREATE TABLE [dbo].[VeritasConfigMeter] (
    [VeritasConfigMeterId] INT            IDENTITY (1, 1) NOT NULL,
    [MeterCount]           INT            NULL,
    [Name]                 NVARCHAR (100) NULL,
    [Status]               INT            NULL,
    [LastUpdated]          DATETIME       CONSTRAINT [DF_VeritasConfigMeter_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]          DATETIME       CONSTRAINT [DF_VeritasConfigMeter_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigMeter] PRIMARY KEY CLUSTERED ([VeritasConfigMeterId] ASC)
);

