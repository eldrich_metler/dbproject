﻿CREATE TABLE [dbo].[AuditDetail] (
    [AuditDetailId] INT            IDENTITY (1, 1) NOT NULL,
    [AuditId]       INT            NULL,
    [ColumnName]    NVARCHAR (500) NULL,
    [OldValue]      NVARCHAR (MAX) NULL,
    [NewValue]      NVARCHAR (MAX) NULL,
    [DateCreated]   DATETIME       NULL,
    [LastUpdated]   DATETIME       NULL,
    CONSTRAINT [PK_AuditDetail] PRIMARY KEY CLUSTERED ([AuditDetailId] ASC),
    CONSTRAINT [FK_AuditDetail_Audit] FOREIGN KEY ([AuditId]) REFERENCES [dbo].[Audit] ([AuditId])
);

