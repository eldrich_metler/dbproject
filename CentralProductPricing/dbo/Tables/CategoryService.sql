﻿CREATE TABLE [dbo].[CategoryService] (
    [ServiceId]  INT NOT NULL,
    [CategoryId] INT NOT NULL,
    CONSTRAINT [FK_ServiceCategories_Category] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[Category] ([CategoryId]),
    CONSTRAINT [FK_ServiceCategories_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

