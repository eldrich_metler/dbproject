﻿CREATE TABLE [dbo].[SophosConfigProductType] (
    [SophosConfigProductTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                      NVARCHAR (100) NULL,
    [Status]                    INT            NULL,
    [LastUpdated]               DATETIME       CONSTRAINT [DF_SophosConfigProductType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]               DATETIME       CONSTRAINT [DF_SophosConfigProductType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SophosConfigProductType] PRIMARY KEY CLUSTERED ([SophosConfigProductTypeId] ASC)
);

