﻿CREATE TABLE [dbo].[SamsungConfigPackage] (
    [SamsungConfigPackageId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (100) NULL,
    [Status]                 INT            NULL,
    [LastUpdated]            DATETIME       CONSTRAINT [DF_SamsungConfigPackage_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]            DATETIME       CONSTRAINT [DF_SamsungConfigPackage_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigPackage] PRIMARY KEY CLUSTERED ([SamsungConfigPackageId] ASC)
);

