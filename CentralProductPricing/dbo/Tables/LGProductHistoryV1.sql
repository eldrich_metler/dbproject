﻿CREATE TABLE [dbo].[LGProductHistoryV1] (
    [LGProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [LGProductId]        INT             NULL,
    [UserAccountId]      INT             NULL,
    [Price]              DECIMAL (10, 2) NULL,
    [DateCreated]        DATETIME        NULL,
    [LastUpdated]        DATETIME        NULL
);

