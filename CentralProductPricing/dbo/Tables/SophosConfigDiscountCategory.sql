﻿CREATE TABLE [dbo].[SophosConfigDiscountCategory] (
    [SophosConfigDiscountCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                           NVARCHAR (100) NULL,
    [Status]                         INT            NULL,
    [LastUpdated]                    DATETIME       CONSTRAINT [DF_SophosConfigDiscountCategory_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]                    DATETIME       CONSTRAINT [DF_SophosConfigDiscountCategory_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SophosConfigDiscountCategory] PRIMARY KEY CLUSTERED ([SophosConfigDiscountCategoryId] ASC)
);

