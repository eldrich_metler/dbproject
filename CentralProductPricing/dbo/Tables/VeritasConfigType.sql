﻿CREATE TABLE [dbo].[VeritasConfigType] (
    [VeritasConfigTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (100) NULL,
    [Status]              INT            NULL,
    [LastUpdated]         DATETIME       CONSTRAINT [DF_VeritasConfigType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]         DATETIME       CONSTRAINT [DF_VeritasConfigType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigType] PRIMARY KEY CLUSTERED ([VeritasConfigTypeId] ASC)
);

