﻿CREATE TABLE [dbo].[ConfigPluginType] (
    [ConfigPluginTypeId] INT          IDENTITY (1, 1) NOT NULL,
    [PluginTypeName]     VARCHAR (25) NOT NULL,
    [DateCreated]        DATETIME     NOT NULL,
    [LastUpdated]        DATETIME     NOT NULL,
    [Status]             INT          CONSTRAINT [DF_ConfigPluginType_Status] DEFAULT ((1)) NULL,
    CONSTRAINT [PK_ConfigPluginType] PRIMARY KEY CLUSTERED ([ConfigPluginTypeId] ASC)
);

