﻿CREATE TABLE [dbo].[DomainsProductTLD] (
    [DomainsProductTLDId] INT             IDENTITY (1, 1) NOT NULL,
    [DomainsProductId]    INT             NOT NULL,
    [DomainsConfigTLDId]  INT             NOT NULL,
    [Name]                NVARCHAR (200)  NULL,
    [SKU]                 NVARCHAR (50)   NOT NULL,
    [Status]              INT             NULL,
    [DateCreated]         DATETIME        NULL,
    [LastUpdated]         DATETIME        NULL,
    [Price]               DECIMAL (10, 2) NULL,
    CONSTRAINT [PK_DomainsProductTLD] PRIMARY KEY CLUSTERED ([DomainsProductTLDId] ASC),
    CONSTRAINT [FK_DomainsProductTLD_DomainsConfigTLD] FOREIGN KEY ([DomainsConfigTLDId]) REFERENCES [dbo].[DomainsConfigTLD] ([DomainsConfigTLDId]),
    CONSTRAINT [FK_DomainsProductTLD_DomainsProductId] FOREIGN KEY ([DomainsProductId]) REFERENCES [dbo].[DomainsProduct] ([DomainsProductId])
);

