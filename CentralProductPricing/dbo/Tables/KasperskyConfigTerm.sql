﻿CREATE TABLE [dbo].[KasperskyConfigTerm] (
    [KasperskyConfigTermId] INT      IDENTITY (1, 1) NOT NULL,
    [Term]                  INT      NOT NULL,
    [ConfigPeriodId]        INT      NOT NULL,
    [Status]                INT      NOT NULL,
    [LastUpdated]           DATETIME NOT NULL,
    [DateCreated]           DATETIME NOT NULL,
    CONSTRAINT [PK_KasperskyConfigTerm] PRIMARY KEY CLUSTERED ([KasperskyConfigTermId] ASC)
);

