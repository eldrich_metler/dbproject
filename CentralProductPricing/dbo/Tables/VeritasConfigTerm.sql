﻿CREATE TABLE [dbo].[VeritasConfigTerm] (
    [VeritasConfigTermId] INT      IDENTITY (1, 1) NOT NULL,
    [Term]                INT      NULL,
    [ConfigPeriodId]      INT      NULL,
    [Status]              INT      NULL,
    [LastUpdated]         DATETIME NULL,
    [DateCreated]         DATETIME NULL,
    CONSTRAINT [PK_VeritasConfigTerm] PRIMARY KEY CLUSTERED ([VeritasConfigTermId] ASC),
    CONSTRAINT [FK_VeritasConfigTerm_ConfigPeriod] FOREIGN KEY ([ConfigPeriodId]) REFERENCES [dbo].[ConfigPeriod] ([ConfigPeriodId])
);

