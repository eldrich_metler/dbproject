﻿CREATE TABLE [dbo].[ZimbraProductBulkUpload] (
    [ZimbraProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [DateCreated]               DATETIME       NULL,
    [LastUpdated]               DATETIME       NULL,
    [Status]                    INT            NULL,
    [BrandId]                   INT            NULL,
    [UserAccountId]             INT            NULL,
    [Name]                      NVARCHAR (50)  NULL,
    [FileName]                  NVARCHAR (150) NULL,
    [ErrorRows]                 NVARCHAR (MAX) NULL,
    [Override]                  BIT            CONSTRAINT [DF_ZimbraProductBulkUpload_Override] DEFAULT ((0)) NOT NULL,
    [DateStarted]               DATETIME       NULL,
    CONSTRAINT [PK_ZimbraProductBulkUpload] PRIMARY KEY CLUSTERED ([ZimbraProductBulkUploadId] ASC),
    CONSTRAINT [FK_ZimbraProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_ZimbraProductBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

