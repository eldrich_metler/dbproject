﻿CREATE TABLE [dbo].[VeeamProduct] (
    [VeeamProductId]                        INT             IDENTITY (1, 1) NOT NULL,
    [Category]                              NVARCHAR (150)  NULL,
    [SubCategory]                           NVARCHAR (150)  NULL,
    [ProductType]                           NVARCHAR (50)   NULL,
    [SKUType]                               NVARCHAR (50)   NULL,
    [LicensedUnit]                          NVARCHAR (50)   NULL,
    [SKU]                                   NVARCHAR (50)   NULL,
    [Description]                           NVARCHAR (MAX)  NULL,
    [Comments]                              NVARCHAR (MAX)  NULL,
    [Discount]                              DECIMAL (6, 2)  NULL,
    [DistiListPrice]                        DECIMAL (12, 2) NULL,
    [DistiDiscount]                         DECIMAL (6, 2)  NULL,
    [DistiPurchasePrice]                    DECIMAL (12, 2) NULL,
    [DistiWithDealRegSixPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTenPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTwelvePercentDiscount] DECIMAL (12, 2) NULL,
    [DateCreated]                           DATETIME        NULL,
    [LastUpdated]                           DATETIME        NULL,
    [Status]                                INT             NULL,
    CONSTRAINT [PK_VeeamProduct] PRIMARY KEY CLUSTERED ([VeeamProductId] ASC)
);

