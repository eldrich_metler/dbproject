﻿CREATE TABLE [dbo].[DomainsConfigType] (
    [DomainsConfigTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                NVARCHAR (100) NULL,
    [Status]              INT            NULL,
    [LastUpdated]         DATETIME       CONSTRAINT [DF_DomainsConfigType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]         DATETIME       CONSTRAINT [DF_DomainsConfigType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_DomainsConfigType] PRIMARY KEY CLUSTERED ([DomainsConfigTypeId] ASC)
);

