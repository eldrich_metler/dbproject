﻿CREATE TABLE [dbo].[LGConfigSegV1] (
    [LGConfigSegId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (50) NULL,
    [Status]        INT           NULL,
    [LastUpdated]   DATETIME      CONSTRAINT [DF_LGConfigSeg_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]   DATETIME      CONSTRAINT [DF_LGConfigSeg_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_LGConfigSeg] PRIMARY KEY CLUSTERED ([LGConfigSegId] ASC)
);

