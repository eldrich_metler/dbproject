﻿CREATE TABLE [dbo].[UserAccountBrand] (
    [UserBrandId]   INT IDENTITY (1, 1) NOT NULL,
    [UserAccountId] INT NULL,
    [BrandId]       INT NULL,
    CONSTRAINT [PK_UserRole_1] PRIMARY KEY CLUSTERED ([UserBrandId] ASC),
    CONSTRAINT [FK_UserBrand_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_UserBrand_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

