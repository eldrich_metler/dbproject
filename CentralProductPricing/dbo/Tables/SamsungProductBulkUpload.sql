﻿CREATE TABLE [dbo].[SamsungProductBulkUpload] (
    [SamsungProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                    INT            NULL,
    [UserAccountId]              INT            NULL,
    [Name]                       NVARCHAR (50)  NULL,
    [FileName]                   NVARCHAR (150) NULL,
    [Status]                     INT            NULL,
    [DateCreated]                DATETIME       NULL,
    [LastUpdated]                DATETIME       NULL,
    [ErrorRows]                  VARCHAR (MAX)  NULL,
    [Override]                   BIT            NOT NULL,
    [DateStarted]                DATETIME       NULL,
    CONSTRAINT [PK_SamsungProductBulkUpload] PRIMARY KEY CLUSTERED ([SamsungProductBulkUploadId] ASC),
    CONSTRAINT [FK_SamsungProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_SamsungProductBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

