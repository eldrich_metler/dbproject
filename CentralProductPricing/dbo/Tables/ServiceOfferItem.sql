﻿CREATE TABLE [dbo].[ServiceOfferItem] (
    [ServiceOfferItemId] INT      IDENTITY (1, 1) NOT NULL,
    [ServiceId]          INT      NOT NULL,
    [ServiceOfferId]     INT      NOT NULL,
    [Status]             INT      NULL,
    [DateCreated]        DATETIME NOT NULL,
    [DateModified]       DATETIME NOT NULL,
    [DateDeleted]        DATETIME NULL,
    CONSTRAINT [PK_ServiceOfferItem] PRIMARY KEY CLUSTERED ([ServiceOfferItemId] ASC),
    CONSTRAINT [FK_ServiceOfferItem_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId]),
    CONSTRAINT [FK_ServiceOfferItem_ServiceOffer] FOREIGN KEY ([ServiceOfferId]) REFERENCES [dbo].[ServiceOffer] ([ServiceOfferId])
);

