﻿CREATE TABLE [dbo].[VeeamProductBulkUpload] (
    [VeeamProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                  INT            NULL,
    [UserAccountId]            INT            NULL,
    [Name]                     NVARCHAR (50)  NULL,
    [FileName]                 NVARCHAR (150) NULL,
    [ErrorRows]                NVARCHAR (MAX) NULL,
    [Status]                   INT            NULL,
    [DateCreated]              DATETIME       NULL,
    [LastUpdated]              DATETIME       NULL,
    [Override]                 BIT            NULL,
    [DateStarted]              DATETIME       NULL,
    CONSTRAINT [PK_VeeamProductBulkUpload] PRIMARY KEY CLUSTERED ([VeeamProductBulkUploadId] ASC),
    CONSTRAINT [FK_VeeamProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_VeeamProductBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

