﻿CREATE TABLE [dbo].[SophosProductBulkUploadItem] (
    [SophosProductBulkUploadItemId] INT             IDENTITY (1, 1) NOT NULL,
    [SophosProductBulkUploadId]     INT             NULL,
    [ProductFamily]                 VARCHAR (100)   NULL,
    [ProductType]                   VARCHAR (100)   NULL,
    [Sector]                        VARCHAR (100)   NULL,
    [DealType]                      VARCHAR (100)   NULL,
    [Units]                         VARCHAR (100)   NULL,
    [Term]                          INT             NULL,
    [Description]                   VARCHAR (300)   NULL,
    [SKU]                           NVARCHAR (10)   NULL,
    [Price]                         DECIMAL (12, 2) NULL,
    [Status]                        INT             NULL,
    [LastUpdated]                   DATETIME        NULL,
    [DateCreated]                   DATETIME        NULL,
    [MinimumQuantity]               INT             NULL,
    [MaximumQuantity]               INT             NULL,
    [StartDate]                     DATETIME        NULL,
    [EndDate]                       DATETIME        NULL,
    CONSTRAINT [PK_SophosProductBulkUploadItem] PRIMARY KEY CLUSTERED ([SophosProductBulkUploadItemId] ASC),
    CONSTRAINT [FK_SophosProductBulkUploadItem_SophosProductBulkUpload] FOREIGN KEY ([SophosProductBulkUploadId]) REFERENCES [dbo].[SophosProductBulkUpload] ([SophosProductBulkUploadId])
);



