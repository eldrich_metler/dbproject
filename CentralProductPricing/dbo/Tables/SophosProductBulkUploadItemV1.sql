﻿CREATE TABLE [dbo].[SophosProductBulkUploadItemV1] (
    [SophosProductBulkUploadItemId]  INT             IDENTITY (1, 1) NOT NULL,
    [SophosProductBulkUploadId]      INT             NULL,
    [SophosConfigCategoryId]         INT             NULL,
    [SophosConfigProductTypeId]      INT             NULL,
    [SophosConfigSectorId]           INT             NULL,
    [SophosConfigDealTypeId]         INT             NULL,
    [SophosConfigTermId]             INT             NULL,
    [SophosConfigDiscountCategoryId] INT             NULL,
    [SophosConfigBandId]             INT             NULL,
    [Name]                           NVARCHAR (200)  NULL,
    [Description]                    NVARCHAR (MAX)  NULL,
    [SKU]                            NVARCHAR (10)   NULL,
    [Price]                          DECIMAL (10, 2) NULL,
    [Status]                         INT             NULL,
    [LastUpdated]                    DATETIME        NULL,
    [DateCreated]                    DATETIME        NULL
);

