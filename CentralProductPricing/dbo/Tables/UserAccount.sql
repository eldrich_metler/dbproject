﻿CREATE TABLE [dbo].[UserAccount] (
    [UserAccountId] INT            IDENTITY (1, 1) NOT NULL,
    [AccountRole]   INT            NULL,
    [Username]      NVARCHAR (50)  NULL,
    [FirstName]     NVARCHAR (100) NULL,
    [LastName]      NVARCHAR (100) NULL,
    [EmailAddress]  NVARCHAR (50)  NULL,
    [PasswordHash]  NVARCHAR (500) NULL,
    [PasswordSalt]  NVARCHAR (500) NULL,
    [CreatedById]   INT            NULL,
    [LastLoggedIn]  DATETIME       NULL,
    [Status]        INT            NULL,
    [DateCreated]   DATETIME       NULL,
    [LastUpdated]   DATETIME       NULL,
    [IsAdmin]       BIT            CONSTRAINT [DF_UserAccount_IsAdmin] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED ([UserAccountId] ASC)
);

