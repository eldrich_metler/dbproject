﻿CREATE TABLE [dbo].[SamsungConfigCategory] (
    [SamsungConfigCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (100) NULL,
    [Status]                  INT            NULL,
    [DateCreated]             DATETIME       NULL,
    [LastUpdated]             DATETIME       NULL,
    CONSTRAINT [PK_SamsungConfigCategory] PRIMARY KEY CLUSTERED ([SamsungConfigCategoryId] ASC),
    CONSTRAINT [FK_SamsungConfigCategory_SamsungConfigCategory] FOREIGN KEY ([SamsungConfigCategoryId]) REFERENCES [dbo].[SamsungConfigCategory] ([SamsungConfigCategoryId])
);

