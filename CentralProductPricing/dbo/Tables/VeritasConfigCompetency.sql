﻿CREATE TABLE [dbo].[VeritasConfigCompetency] (
    [VeritasConfigCompetencyId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                      NVARCHAR (100) NULL,
    [Status]                    INT            NULL,
    [LastUpdated]               DATETIME       CONSTRAINT [DF_VeritasConfigCompetency_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]               DATETIME       CONSTRAINT [DF_VeritasConfigCompetency_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigCompetency] PRIMARY KEY CLUSTERED ([VeritasConfigCompetencyId] ASC)
);

