﻿CREATE TABLE [dbo].[KasperskyProductApplicationExcluded] (
    [KasperskyProductApplicationExcludedId] INT IDENTITY (1, 1) NOT NULL,
    [KasperskyProductId]                    INT NULL,
    [ApplicationId]                         INT NULL,
    PRIMARY KEY CLUSTERED ([KasperskyProductApplicationExcludedId] ASC),
    CONSTRAINT [FK_KasperskyProductApplicationExcluded_Application] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Application] ([ApplicationId]),
    CONSTRAINT [FK_KasperskyProductApplicationExcluded_KasperskyProductId] FOREIGN KEY ([KasperskyProductId]) REFERENCES [dbo].[KasperskyProduct] ([KasperskyProductId])
);

