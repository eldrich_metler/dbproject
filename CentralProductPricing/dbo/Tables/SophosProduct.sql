﻿CREATE TABLE [dbo].[SophosProduct] (
    [SophosProductId] INT             IDENTITY (1, 1) NOT NULL,
    [BrandId]         INT             NULL,
    [Description]     NVARCHAR (300)  NULL,
    [Status]          INT             NULL,
    [DateCreated]     DATETIME        NULL,
    [LastUpdated]     DATETIME        NULL,
    [Units]           VARCHAR (100)   NULL,
    [MinimumQuantity] INT             NULL,
    [MaximumQuantity] INT             NULL,
    [Term]            INT             NULL,
    [Sector]          VARCHAR (100)   NULL,
    [ProductFamily]   VARCHAR (100)   NULL,
    [ProductType]     VARCHAR (100)   NULL,
    [DealType]        VARCHAR (100)   NULL,
    [SKU]             VARCHAR (10)    NULL,
    [Price]           DECIMAL (12, 2) NULL,
    [StartDate]       DATETIME        NULL,
    [EndDate]         DATETIME        NULL,
    CONSTRAINT [PK_SophosProduct] PRIMARY KEY CLUSTERED ([SophosProductId] ASC),
    CONSTRAINT [FK_SophosProduct_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);



