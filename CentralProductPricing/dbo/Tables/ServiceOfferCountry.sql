﻿CREATE TABLE [dbo].[ServiceOfferCountry] (
    [ServiceOfferCountryId] INT             IDENTITY (1, 1) NOT NULL,
    [ServiceOfferId]        INT             NOT NULL,
    [CountryId]             INT             NOT NULL,
    [Name]                  NVARCHAR (250)  NULL,
    [Description]           NVARCHAR (2000) NULL,
    [Status]                INT             NULL,
    [DateCreated]           DATETIME        NOT NULL,
    [DateModified]          DATETIME        NOT NULL,
    [DateDeleted]           DATETIME        NULL,
    CONSTRAINT [PK_ServiceOfferCountry] PRIMARY KEY CLUSTERED ([ServiceOfferCountryId] ASC),
    CONSTRAINT [FK_ServiceOfferCountry_Country] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([CountryId]),
    CONSTRAINT [FK_ServiceOfferCountry_ServiceOffer] FOREIGN KEY ([ServiceOfferId]) REFERENCES [dbo].[ServiceOffer] ([ServiceOfferId])
);

