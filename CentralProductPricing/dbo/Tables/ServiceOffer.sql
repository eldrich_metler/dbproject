﻿CREATE TABLE [dbo].[ServiceOffer] (
    [ServiceOfferId]        INT             IDENTITY (1, 1) NOT NULL,
    [BrandId]               INT             NOT NULL,
    [Name]                  NVARCHAR (150)  NOT NULL,
    [Description]           NVARCHAR (500)  NULL,
    [MinimumQuantity]       INT             NOT NULL,
    [IsProRate]             BIT             NOT NULL,
    [PricingBillingCycleId] INT             NOT NULL,
    [VendorSKU]             NVARCHAR (100)  NOT NULL,
    [DistributorSKU]        NVARCHAR (100)  NULL,
    [Usage]                 INT             NULL,
    [UnitOfMeasureId]       INT             NOT NULL,
    [ItemOrder]             INT             NULL,
    [Source]                INT             NULL,
    [VendorPrice]           DECIMAL (18, 2) NULL,
    [ResellerPrice]         DECIMAL (18, 2) NULL,
    [ResellerMargin]        DECIMAL (18, 2) NULL,
    [Status]                INT             NOT NULL,
    [DateCreated]           DATETIME        NOT NULL,
    [DateModified]          DATETIME        NOT NULL,
    [DateArchived]          DATETIME        NULL,
    CONSTRAINT [PK_ServiceOffer] PRIMARY KEY CLUSTERED ([ServiceOfferId] ASC),
    CONSTRAINT [FK_ServiceOffer_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_ServiceOffer_PricingBillingCycle] FOREIGN KEY ([PricingBillingCycleId]) REFERENCES [dbo].[PricingBillingCycle] ([PricingBillingCycleId]),
    CONSTRAINT [FK_ServiceOffer_UnitOfMeasure] FOREIGN KEY ([UnitOfMeasureId]) REFERENCES [dbo].[UnitOfMeasure] ([UnitOfMeasureId])
);





