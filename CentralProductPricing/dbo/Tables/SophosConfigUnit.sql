﻿CREATE TABLE [dbo].[SophosConfigUnit] (
    [SophosConfigUnitId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (100) NULL,
    [Status]             INT            NULL,
    [LastUpdated]        DATETIME       CONSTRAINT [DF_SophosConfigUnit_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]        DATETIME       CONSTRAINT [DF_SophosConfigUnit_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SophosConfigUnit] PRIMARY KEY CLUSTERED ([SophosConfigUnitId] ASC)
);

