﻿CREATE TABLE [dbo].[VeritasProductHistory] (
    [VeritasProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [UserAccountId]           INT             NULL,
    [Price]                   DECIMAL (10, 2) NULL,
    [DateCreated]             DATETIME        NULL,
    [LastUpdated]             DATETIME        NULL,
    [PartnerBuyPrice]         DECIMAL (10, 2) NULL,
    [VeritasProductId]        INT             NULL,
    CONSTRAINT [PK_VeritasProductHistory] PRIMARY KEY CLUSTERED ([VeritasProductHistoryId] ASC),
    CONSTRAINT [FK_VeritasProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    CONSTRAINT [FK_VeritasProductHistory_VeritasProduct] FOREIGN KEY ([VeritasProductId]) REFERENCES [dbo].[VeritasProduct] ([VeritasProductId])
);



