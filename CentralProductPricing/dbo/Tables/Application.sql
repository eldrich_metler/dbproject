﻿CREATE TABLE [dbo].[Application] (
    [ApplicationId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]          NVARCHAR (100) NULL,
    [Status]        INT            NULL,
    [LastUpdated]   DATETIME       NULL,
    [DateCreated]   DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([ApplicationId] ASC)
);

