﻿CREATE TABLE [dbo].[Brand] (
    [BrandId]               INT             IDENTITY (1, 1) NOT NULL,
    [Name]                  VARCHAR (50)    NOT NULL,
    [Description]           NVARCHAR (MAX)  NULL,
    [BrandEmailAddressList] NVARCHAR (4000) NULL,
    [ConfigCurrencyId]      INT             NULL,
    [Status]                INT             NULL,
    [LastUpdated]           DATETIME        NULL,
    [DateCreated]           DATETIME        NULL,
    [TermsAndConditions]    NVARCHAR (MAX)  NULL,
    [FileName]              NVARCHAR (50)   NULL,
    [MainLogo]              VARBINARY (MAX) NULL,
    [Color]                 VARCHAR (7)     DEFAULT ('#007BFF') NOT NULL,
    CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED ([BrandId] ASC),
    CONSTRAINT [FK_Brand_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId])
);

