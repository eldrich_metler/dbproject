﻿CREATE TABLE [dbo].[KasperskyConfigCategory] (
    [KasperskyConfigCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [ParentKasperskyCategoryId] INT            NULL,
    [Name]                      NVARCHAR (100) NULL,
    [Order]                     INT            NULL,
    [Depth]                     INT            NULL,
    [Status]                    INT            NULL,
    [DateCreated]               DATETIME       NULL,
    [LastUpdated]               DATETIME       NULL,
    CONSTRAINT [PK_KasperskyConfigCategory] PRIMARY KEY CLUSTERED ([KasperskyConfigCategoryId] ASC),
    CONSTRAINT [FK_KasperskyConfigCategory_KasperskyConfigCategory] FOREIGN KEY ([ParentKasperskyCategoryId]) REFERENCES [dbo].[KasperskyConfigCategory] ([KasperskyConfigCategoryId])
);

