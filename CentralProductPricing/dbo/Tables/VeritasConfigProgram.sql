﻿CREATE TABLE [dbo].[VeritasConfigProgram] (
    [VeritasConfigProgramId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (100) NULL,
    [Status]                 INT            NULL,
    [LastUpdated]            DATETIME       CONSTRAINT [DF_VeritasConfigProgram_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]            DATETIME       CONSTRAINT [DF_VeritasConfigProgram_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigProgram] PRIMARY KEY CLUSTERED ([VeritasConfigProgramId] ASC)
);

