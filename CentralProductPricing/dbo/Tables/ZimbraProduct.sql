﻿CREATE TABLE [dbo].[ZimbraProduct] (
    [ZimbraProductId]       INT            IDENTITY (1, 1) NOT NULL,
    [ZimbraPriceBookId]     INT            NULL,
    [ZimbraEditionId]       INT            NULL,
    [ZimbraLicenseTypeId]   INT            NULL,
    [ZimbraTermId]          INT            NULL,
    [SupportIncluded]       BIT            CONSTRAINT [DF_ZimbraProduct_SupportIncluded] DEFAULT ((0)) NOT NULL,
    [ZimbraSupportTypeId]   INT            NULL,
    [Mobile]                BIT            CONSTRAINT [DF_ZimbraProduct_Mobile] DEFAULT ((0)) NOT NULL,
    [ArchivingAndDiscovery] BIT            CONSTRAINT [DF_ZimbraProduct_ArchivingAndDiscovery] DEFAULT ((0)) NOT NULL,
    [Description]           NVARCHAR (250) NULL,
    [DateCreated]           DATETIME       NULL,
    [LastUpdated]           DATETIME       NULL,
    [Status]                INT            NULL,
    CONSTRAINT [PK_ZimbraProduct] PRIMARY KEY CLUSTERED ([ZimbraProductId] ASC),
    CONSTRAINT [FK_ZimbraProduct_ZimbraEdition] FOREIGN KEY ([ZimbraEditionId]) REFERENCES [dbo].[ZimbraEdition] ([ZimbraEditionId]),
    CONSTRAINT [FK_ZimbraProduct_ZimbraLicenseType] FOREIGN KEY ([ZimbraLicenseTypeId]) REFERENCES [dbo].[ZimbraLicenseType] ([ZimbraLicenseTypeId]),
    CONSTRAINT [FK_ZimbraProduct_ZimbraPriceBook] FOREIGN KEY ([ZimbraPriceBookId]) REFERENCES [dbo].[ZimbraPriceBook] ([ZimbraPriceBookId]),
    CONSTRAINT [FK_ZimbraProduct_ZimbraSupportType] FOREIGN KEY ([ZimbraSupportTypeId]) REFERENCES [dbo].[ZimbraSupportType] ([ZimbraSupportTypeId]),
    CONSTRAINT [FK_ZimbraProduct_ZimbraTerm] FOREIGN KEY ([ZimbraTermId]) REFERENCES [dbo].[ZimbraTerm] ([ZimbraTermId])
);

