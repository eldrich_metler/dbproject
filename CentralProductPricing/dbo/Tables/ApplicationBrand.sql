﻿CREATE TABLE [dbo].[ApplicationBrand] (
    [ApplicationBrandId] INT IDENTITY (1, 1) NOT NULL,
    [ApplicationId]      INT NULL,
    [BrandId]            INT NULL,
    PRIMARY KEY CLUSTERED ([ApplicationBrandId] ASC),
    CONSTRAINT [FK_ApplicationBrand_Appilcation] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[Application] ([ApplicationId]),
    CONSTRAINT [FK_ApplicationBrand_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);

