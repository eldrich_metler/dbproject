﻿CREATE TABLE [dbo].[LifesGoodProductHistory] (
    [LifesGoodProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [LifesGoodProductId]        INT             NULL,
    [UserAccountId]             INT             NULL,
    [Price]                     DECIMAL (12, 2) NULL,
    [DateCreated]               DATETIME        NULL,
    [LastUpdated]               DATETIME        NULL,
    CONSTRAINT [PK_LifesGoodProductHistory] PRIMARY KEY CLUSTERED ([LifesGoodProductHistoryId] ASC),
    CONSTRAINT [FK_LifesGoodProductHistory_LifesGoodProduct] FOREIGN KEY ([LifesGoodProductId]) REFERENCES [dbo].[LifesGoodProduct] ([LifesGoodProductId]),
    CONSTRAINT [FK_LifesGoodProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

