﻿CREATE TABLE [dbo].[DomainsProduct] (
    [DomainsProductId]    INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]             INT            NULL,
    [DomainsConfigTypeId] INT            NULL,
    [DomainsConfigTermId] INT            NULL,
    [Name]                NVARCHAR (200) NULL,
    [Description]         NVARCHAR (MAX) NULL,
    [Status]              INT            NULL,
    [DateCreated]         DATETIME       NULL,
    [LastUpdated]         DATETIME       NULL,
    CONSTRAINT [PK_DomainsProduct] PRIMARY KEY CLUSTERED ([DomainsProductId] ASC),
    CONSTRAINT [FK_DomainsProduct_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_DomainsProduct_DomainsConfigTerm] FOREIGN KEY ([DomainsConfigTermId]) REFERENCES [dbo].[DomainsConfigTerm] ([DomainsConfigTermId]),
    CONSTRAINT [FK_DomainsProduct_DomainsConfigType] FOREIGN KEY ([DomainsConfigTypeId]) REFERENCES [dbo].[DomainsConfigType] ([DomainsConfigTypeId])
);

