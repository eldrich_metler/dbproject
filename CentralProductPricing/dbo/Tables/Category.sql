﻿CREATE TABLE [dbo].[Category] (
    [CategoryId]       INT            IDENTITY (1, 1) NOT NULL,
    [ParentCategoryId] INT            NULL,
    [Name]             NVARCHAR (100) NULL,
    [ItemOrder]        INT            NULL,
    [Depth]            INT            NULL,
    [Status]           INT            NOT NULL,
    [DateCreated]      DATETIME       NULL,
    [DateModified]     DATETIME       NULL,
    [DateDeleted]      DATETIME       NULL,
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([CategoryId] ASC)
);

