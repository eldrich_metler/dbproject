﻿CREATE TABLE [dbo].[SamsungConfigLicence] (
    [SamsungConfigLicenceId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (50) NULL,
    [Status]                 INT           NULL,
    [LastUpdated]            DATETIME      CONSTRAINT [DF_SamsungConfigLicence_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]            DATETIME      CONSTRAINT [DF_SamsungConfigLicence_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigLicence] PRIMARY KEY CLUSTERED ([SamsungConfigLicenceId] ASC)
);

