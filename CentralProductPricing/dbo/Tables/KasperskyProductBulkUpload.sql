﻿CREATE TABLE [dbo].[KasperskyProductBulkUpload] (
    [KasperskyProductBulkUploadId] INT           IDENTITY (1, 1) NOT NULL,
    [BrandId]                      INT           NULL,
    [UserAccountId]                INT           NULL,
    [Name]                         NVARCHAR (50) NULL,
    [DateCreated]                  DATETIME      NULL,
    [LastUpdated]                  DATETIME      NULL,
    [Status]                       INT           NULL,
    [FileName]                     VARCHAR (150) NULL,
    [ErrorRows]                    VARCHAR (MAX) NULL,
    [Override]                     BIT           NOT NULL,
    [DateStarted]                  DATETIME      NULL,
    CONSTRAINT [PK_KasperskyProductBulkUpload] PRIMARY KEY CLUSTERED ([KasperskyProductBulkUploadId] ASC),
    CONSTRAINT [FK_KasperskyProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);

