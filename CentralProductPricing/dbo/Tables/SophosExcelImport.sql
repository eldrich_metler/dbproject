﻿CREATE TABLE [dbo].[SophosExcelImport] (
    [SophosExcelImportId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (50)  NOT NULL,
    [FileName]            VARCHAR (255) NOT NULL,
    [DateUploaded]        DATETIME      NOT NULL,
    [Status]              INT           NOT NULL,
    [UserAccountId]       INT           NOT NULL,
    [Inserted]            INT           NULL,
    [Updated]             INT           NULL,
    [Ignored]             INT           NULL,
    [ErrorRows]           VARCHAR (MAX) NULL,
    CONSTRAINT [FK_SophosExcelImport_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

