﻿CREATE TABLE [dbo].[LifesGoodProduct] (
    [LifesGoodProductId] INT             IDENTITY (1, 1) NOT NULL,
    [Seg]                NVARCHAR (50)   NULL,
    [Series]             NVARCHAR (20)   NULL,
    [Inch]               INT             NULL,
    [Model]              NVARCHAR (20)   NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    [Price]              DECIMAL (12, 2) NULL,
    [Status]             INT             NULL,
    [DateCreated]        DATETIME        NULL,
    [LastUpdated]        DATETIME        NULL,
    CONSTRAINT [PK_LifesGoodProduct] PRIMARY KEY CLUSTERED ([LifesGoodProductId] ASC)
);

