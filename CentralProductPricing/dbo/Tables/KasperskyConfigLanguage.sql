﻿CREATE TABLE [dbo].[KasperskyConfigLanguage] (
    [KasperskyConfigLanguageId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                      NVARCHAR (50) NULL,
    [ShortDescription]          NVARCHAR (10) NULL,
    [Status]                    INT           NULL,
    [DateCreated]               DATETIME      NULL,
    [LastUpdated]               DATETIME      NULL,
    CONSTRAINT [PK_KasperskyConfigLanguage] PRIMARY KEY CLUSTERED ([KasperskyConfigLanguageId] ASC)
);

