﻿CREATE TABLE [dbo].[SamsungProductHistory] (
    [SamsungProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [SamsungProductId]        INT             NULL,
    [UserAccountId]           INT             NULL,
    [Price]                   DECIMAL (10, 2) NULL,
    [DateCreated]             DATETIME        NULL,
    [LastUpdated]             DATETIME        NULL,
    CONSTRAINT [PK_SamsungProductHistory] PRIMARY KEY CLUSTERED ([SamsungProductHistoryId] ASC),
    CONSTRAINT [FK_SamsungProductHistory_SamsungProduct] FOREIGN KEY ([SamsungProductId]) REFERENCES [dbo].[SamsungProduct] ([SamsungProductId])
);

