﻿CREATE TABLE [dbo].[ServiceFile] (
    [ServiceFileId] INT             IDENTITY (1, 1) NOT NULL,
    [ServiceId]     INT             NOT NULL,
    [Name]          NVARCHAR (100)  NULL,
    [BinaryData]    VARBINARY (MAX) NOT NULL,
    [Format]        INT             NULL,
    [Status]        INT             NULL,
    [Type]          INT             NULL,
    [DateCreated]   DATETIME        NULL,
    [DateModified]  DATETIME        NULL,
    [DateDeleted]   DATETIME        NULL,
    CONSTRAINT [PK_ServiceFile] PRIMARY KEY CLUSTERED ([ServiceFileId] ASC),
    CONSTRAINT [FK_ServiceFile_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

