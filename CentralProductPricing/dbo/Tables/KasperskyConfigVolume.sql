﻿CREATE TABLE [dbo].[KasperskyConfigVolume] (
    [KasperskyConfigVolumeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (20) NULL,
    [Status]                  INT           NULL,
    [LastUpdated]             DATETIME      CONSTRAINT [DF_KasperskyConfigVolume_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]             DATETIME      CONSTRAINT [DF_KasperskyConfigVolume_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_KasperskyConfigVolume] PRIMARY KEY CLUSTERED ([KasperskyConfigVolumeId] ASC)
);

