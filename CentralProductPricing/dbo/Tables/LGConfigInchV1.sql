﻿CREATE TABLE [dbo].[LGConfigInchV1] (
    [LGConfigInchId] INT      IDENTITY (1, 1) NOT NULL,
    [Inch]           INT      NULL,
    [Status]         INT      NULL,
    [LastUpdated]    DATETIME CONSTRAINT [DF_LGConfigInch_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]    DATETIME CONSTRAINT [DF_LGConfigInch_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_LGConfigInch] PRIMARY KEY CLUSTERED ([LGConfigInchId] ASC)
);

