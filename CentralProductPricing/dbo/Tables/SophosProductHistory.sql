﻿CREATE TABLE [dbo].[SophosProductHistory] (
    [SophosProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [SophosProductId]        INT             NULL,
    [UserAccountId]          INT             NULL,
    [SophosProductBandId]    INT             NULL,
    [Price]                  DECIMAL (12, 2) NULL,
    [DateCreated]            DATETIME        NULL,
    [LastUpdated]            DATETIME        NULL,
    CONSTRAINT [PK_SophosProductHistory] PRIMARY KEY CLUSTERED ([SophosProductHistoryId] ASC),
    CONSTRAINT [FK_SophosProductHistory_SophosProduct] FOREIGN KEY ([SophosProductId]) REFERENCES [dbo].[SophosProduct] ([SophosProductId]),
    CONSTRAINT [FK_SophosProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

