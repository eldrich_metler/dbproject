﻿CREATE TABLE [dbo].[KasperskyProductHistory] (
    [KasperskyProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [KasperskyProductId]        INT             NULL,
    [UserAccountId]             INT             NULL,
    [Price]                     DECIMAL (10, 2) NULL,
    [DateCreated]               DATETIME        NULL,
    [LastUpdated]               DATETIME        NULL,
    CONSTRAINT [PK_KasperskyProductHistory] PRIMARY KEY CLUSTERED ([KasperskyProductHistoryId] ASC),
    CONSTRAINT [FK_KasperskyProductHistory_KasperskyProduct] FOREIGN KEY ([KasperskyProductId]) REFERENCES [dbo].[KasperskyProduct] ([KasperskyProductId]),
    CONSTRAINT [FK_KasperskyProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

