﻿CREATE TABLE [dbo].[SamsungConfigCoaxial] (
    [SamsungConfigCoaxialId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (100) NULL,
    [Status]                 INT            NULL,
    [LastUpdated]            DATETIME       CONSTRAINT [DF_SamsungConfigCoaxial_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]            DATETIME       CONSTRAINT [DF_SamsungConfigCoaxial_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigCoaxial] PRIMARY KEY CLUSTERED ([SamsungConfigCoaxialId] ASC)
);

