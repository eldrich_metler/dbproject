﻿CREATE TABLE [dbo].[SophosProductBand] (
    [SophosProductBandId] INT             IDENTITY (1, 1) NOT NULL,
    [SophosProductId]     INT             NULL,
    [SophosConfigBandId]  INT             NULL,
    [Price]               DECIMAL (10, 2) NULL,
    [SKU]                 NVARCHAR (10)   NULL,
    [Status]              INT             NULL,
    [DateCreated]         DATETIME        NULL,
    [LastUpdated]         DATETIME        NULL,
    [SophosConfigUnitId]  INT             NOT NULL,
    CONSTRAINT [PK_SophosProductBand] PRIMARY KEY CLUSTERED ([SophosProductBandId] ASC)
);



