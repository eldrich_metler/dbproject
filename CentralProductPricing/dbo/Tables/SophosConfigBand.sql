﻿CREATE TABLE [dbo].[SophosConfigBand] (
    [SophosConfigBandId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (50) NULL,
    [MinimumQuantity]    INT           NULL,
    [MaximumQuantity]    INT           NULL,
    [Status]             INT           NULL,
    [LastUpdated]        DATETIME      NULL,
    [DateCreated]        DATETIME      NOT NULL,
    CONSTRAINT [PK_SophosConfigBand] PRIMARY KEY CLUSTERED ([SophosConfigBandId] ASC)
);

