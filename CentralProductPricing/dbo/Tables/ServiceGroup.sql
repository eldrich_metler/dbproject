﻿CREATE TABLE [dbo].[ServiceGroup] (
    [ServiceGroupId] INT             IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (250)  NULL,
    [Description]    NVARCHAR (2000) NULL,
    [Status]         INT             NULL,
    [DateCreated]    DATETIME        NULL,
    [DateModified]   DATETIME        NULL,
    [DateDeleted]    DATETIME        NULL,
    CONSTRAINT [PK_ServiceGroup] PRIMARY KEY CLUSTERED ([ServiceGroupId] ASC)
);

