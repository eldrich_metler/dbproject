﻿CREATE TABLE [dbo].[DomainsConfigTLD] (
    [DomainsConfigTLDId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]               NVARCHAR (100) NULL,
    [Status]             INT            NULL,
    [LastUpdated]        DATETIME       CONSTRAINT [DF_DomainsConfigTLD_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]        DATETIME       CONSTRAINT [DF_DomainsConfigTLD_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_DomainsConfigTLD] PRIMARY KEY CLUSTERED ([DomainsConfigTLDId] ASC)
);

