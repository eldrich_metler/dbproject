﻿CREATE TABLE [dbo].[SophosConfigCategory] (
    [SophosConfigCategoryId] INT            IDENTITY (1, 1) NOT NULL,
    [ParentSophosCategoryId] INT            NULL,
    [Name]                   NVARCHAR (100) NULL,
    [Order]                  INT            NULL,
    [Depth]                  INT            NULL,
    [Status]                 INT            NULL,
    [DateCreated]            DATETIME       NULL,
    [LastUpdated]            DATETIME       NULL,
    CONSTRAINT [PK_SophosConfigCategory] PRIMARY KEY CLUSTERED ([SophosConfigCategoryId] ASC)
);

