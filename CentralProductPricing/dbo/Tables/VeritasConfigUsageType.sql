﻿CREATE TABLE [dbo].[VeritasConfigUsageType] (
    [VeritasConfigUsageTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (100) NULL,
    [Status]                   INT            NULL,
    [LastUpdated]              DATETIME       CONSTRAINT [DF_VeritasConfigUsageType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]              DATETIME       CONSTRAINT [DF_VeritasConfigUsageType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigUsageType] PRIMARY KEY CLUSTERED ([VeritasConfigUsageTypeId] ASC)
);

