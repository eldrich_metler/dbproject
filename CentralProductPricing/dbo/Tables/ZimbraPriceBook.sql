﻿CREATE TABLE [dbo].[ZimbraPriceBook] (
    [ZimbraPriceBookId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (100) NULL,
    [DateCreated]       DATETIME       NULL,
    [LastUpdated]       DATETIME       NULL,
    [Status]            INT            NULL,
    CONSTRAINT [PK_ZimbraPriceBook] PRIMARY KEY CLUSTERED ([ZimbraPriceBookId] ASC)
);

