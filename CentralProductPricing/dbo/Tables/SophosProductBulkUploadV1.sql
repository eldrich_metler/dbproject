﻿CREATE TABLE [dbo].[SophosProductBulkUploadV1] (
    [SophosProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                   INT            NULL,
    [UserAccountId]             INT            NULL,
    [Name]                      NVARCHAR (50)  NULL,
    [FileName]                  NVARCHAR (150) NULL,
    [Status]                    INT            NULL,
    [DateCreated]               DATETIME       NULL,
    [LastUpdated]               DATETIME       NULL,
    [ErrorRows]                 VARCHAR (MAX)  NULL,
    [Override]                  BIT            NOT NULL
);

