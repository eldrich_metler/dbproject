﻿CREATE TABLE [dbo].[VeeamProductBulkUploadItem] (
    [VeeamProductBulkUploadItemId]          INT             IDENTITY (1, 1) NOT NULL,
    [VeeamProductBulkUploadId]              INT             NULL,
    [Category]                              NVARCHAR (150)  NULL,
    [LicensedUnit]                          NVARCHAR (50)   NULL,
    [ProductType]                           NVARCHAR (50)   NULL,
    [SKUType]                               NVARCHAR (50)   NULL,
    [Description]                           NVARCHAR (MAX)  NULL,
    [Comments]                              NVARCHAR (MAX)  NULL,
    [Discount]                              DECIMAL (5, 2)  NULL,
    [DistiListPrice]                        DECIMAL (12, 2) NULL,
    [DistiDiscount]                         DECIMAL (5, 2)  NULL,
    [DistiPurchasePrice]                    DECIMAL (12, 2) NULL,
    [DistiWithDealRegSixPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTenPercentDiscount]    DECIMAL (12, 2) NULL,
    [DistiWithDealRegTwelvePercentDiscount] DECIMAL (12, 2) NULL,
    [SKU]                                   NVARCHAR (50)   NULL,
    [Status]                                INT             NULL,
    [DateCreated]                           DATETIME        NULL,
    [LastUpdated]                           DATETIME        NULL,
    CONSTRAINT [PK_VeeamProductBulkUploadItem] PRIMARY KEY CLUSTERED ([VeeamProductBulkUploadItemId] ASC),
    CONSTRAINT [FK_VeeamProductBulkUploadItem_VeeamProductBulkUpload] FOREIGN KEY ([VeeamProductBulkUploadId]) REFERENCES [dbo].[VeeamProductBulkUpload] ([VeeamProductBulkUploadId])
);

