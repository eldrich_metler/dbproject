﻿CREATE TABLE [dbo].[ZimbraProductBand] (
    [ZimbraProductBandId] INT             IDENTITY (1, 1) NOT NULL,
    [ZimbraProductId]     INT             NULL,
    [ZimbraBandId]        INT             NULL,
    [ProductCode]         NVARCHAR (50)   NULL,
    [ListPrice]           DECIMAL (12, 2) NULL,
    [ExtendedPrice]       DECIMAL (12, 2) NULL,
    [DateCreated]         DATETIME        NULL,
    [LastUpdated]         DATETIME        NULL,
    [Status]              INT             NULL,
    CONSTRAINT [PK_ZimbraProductBand] PRIMARY KEY CLUSTERED ([ZimbraProductBandId] ASC),
    CONSTRAINT [FK_ZimbraProductBand_ZimbraBand] FOREIGN KEY ([ZimbraBandId]) REFERENCES [dbo].[ZimbraBand] ([ZimbraBandId]),
    CONSTRAINT [FK_ZimbraProductBand_ZimbraProduct] FOREIGN KEY ([ZimbraProductId]) REFERENCES [dbo].[ZimbraProduct] ([ZimbraProductId])
);

