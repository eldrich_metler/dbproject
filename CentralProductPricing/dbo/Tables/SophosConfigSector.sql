﻿CREATE TABLE [dbo].[SophosConfigSector] (
    [SophosConfigSectorId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (100) NULL,
    [Status]               INT            NULL,
    [LastUpdated]          DATETIME       CONSTRAINT [DF_SophosConfigSector_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]          DATETIME       CONSTRAINT [DF_SophosConfigSector_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SophosConfigSector] PRIMARY KEY CLUSTERED ([SophosConfigSectorId] ASC)
);

