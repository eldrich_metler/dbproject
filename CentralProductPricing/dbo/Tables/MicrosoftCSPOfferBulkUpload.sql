﻿CREATE TABLE [dbo].[MicrosoftCSPOfferBulkUpload] (
    [MicrosoftCSPOfferBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                       INT            NULL,
    [UserAccountId]                 INT            NULL,
    [Name]                          NVARCHAR (50)  NULL,
    [DateCreated]                   DATETIME       NULL,
    [LastUpdated]                   DATETIME       NULL,
    [Status]                        INT            NULL,
    [FileName]                      NVARCHAR (50)  NULL,
    [ErrorRows]                     NVARCHAR (MAX) NULL,
    [Override]                      BIT            CONSTRAINT [DF_MicrosoftCSPOfferBulkUpload_Override] DEFAULT ((0)) NULL,
    [DateStarted]                   DATETIME       NULL,
    CONSTRAINT [PK_MicrosoftCSPOfferBulkUpload] PRIMARY KEY CLUSTERED ([MicrosoftCSPOfferBulkUploadId] ASC),
    CONSTRAINT [FK_MicrosoftCSPOfferBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_MicrosoftCSPOfferBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

