﻿CREATE TABLE [dbo].[LifesGoodProductBulkUpload] (
    [LifesGoodProductBulkUploadId] INT            IDENTITY (1, 1) NOT NULL,
    [BrandId]                      INT            NULL,
    [UserAccountId]                INT            NULL,
    [Name]                         NVARCHAR (50)  NULL,
    [FileName]                     NVARCHAR (150) NULL,
    [Status]                       INT            NULL,
    [DateCreated]                  DATETIME       NULL,
    [LastUpdated]                  DATETIME       NULL,
    [ErrorRows]                    NVARCHAR (MAX) NULL,
    [Override]                     BIT            NULL,
    [DateStarted]                  DATETIME       NULL,
    CONSTRAINT [PK_LifesGoodProductBulkUpload] PRIMARY KEY CLUSTERED ([LifesGoodProductBulkUploadId] ASC),
    CONSTRAINT [FK_LifesGoodProductBulkUpload_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_LifesGoodProductBulkUpload_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

