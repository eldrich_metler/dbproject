﻿CREATE TABLE [dbo].[SamsungConfigSeries] (
    [SamsungConfigSeriesId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                  NVARCHAR (20) NULL,
    [Status]                INT           NULL,
    [LastUpdated]           DATETIME      CONSTRAINT [DF_SamsungConfigSeries_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]           DATETIME      CONSTRAINT [DF_SamsungConfigSeries_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigSeries] PRIMARY KEY CLUSTERED ([SamsungConfigSeriesId] ASC)
);

