﻿CREATE TABLE [dbo].[ZimbraTerm] (
    [ZimbraTermId]   INT      IDENTITY (1, 1) NOT NULL,
    [Term]           INT      NULL,
    [DateCreated]    DATETIME NULL,
    [LastUpdated]    DATETIME NULL,
    [Status]         INT      NULL,
    [ConfigPeriodId] INT      NULL,
    CONSTRAINT [PK_ZimbraTerm] PRIMARY KEY CLUSTERED ([ZimbraTermId] ASC),
    CONSTRAINT [FK_ZimbraTerm_ConfigPeriod] FOREIGN KEY ([ConfigPeriodId]) REFERENCES [dbo].[ConfigPeriod] ([ConfigPeriodId])
);

