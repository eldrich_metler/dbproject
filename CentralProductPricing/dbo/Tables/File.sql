﻿CREATE TABLE [dbo].[File] (
    [FileId]      INT             IDENTITY (1, 1) NOT NULL,
    [BrandId]     INT             NULL,
    [Name]        NVARCHAR (50)   NULL,
    [ImageBinary] VARBINARY (MAX) NOT NULL,
    [Format]      INT             NULL,
    [Type]        INT             NULL,
    [Filter]      INT             NULL,
    [Status]      NCHAR (10)      NULL,
    [LastUpdated] DATETIME        NULL,
    [DateCreated] DATETIME        NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([FileId] ASC),
    CONSTRAINT [FK_File_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId])
);

