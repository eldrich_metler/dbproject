﻿CREATE TABLE [dbo].[LifesGoodProductBulkUploadItem] (
    [LifesGoodProductBulkUploadItemId] INT             IDENTITY (1, 1) NOT NULL,
    [LifesGoodProductBulkUploadId]     INT             NULL,
    [Seg]                              NVARCHAR (50)   NULL,
    [Series]                           NVARCHAR (20)   NULL,
    [Inch]                             INT             NULL,
    [Model]                            NVARCHAR (20)   NULL,
    [Description]                      NVARCHAR (MAX)  NULL,
    [Price]                            DECIMAL (12, 2) NULL,
    [Status]                           INT             NULL,
    [DateCreated]                      DATETIME        NULL,
    [LastUpdated]                      DATETIME        NULL,
    CONSTRAINT [PK_LifesGoodProductBulkUploadItem] PRIMARY KEY CLUSTERED ([LifesGoodProductBulkUploadItemId] ASC),
    CONSTRAINT [FK_LifesGoodProductBulkUploadItem_LifesGoodProductBulkUpload] FOREIGN KEY ([LifesGoodProductBulkUploadId]) REFERENCES [dbo].[LifesGoodProductBulkUpload] ([LifesGoodProductBulkUploadId])
);

