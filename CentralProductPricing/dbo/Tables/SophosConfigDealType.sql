﻿CREATE TABLE [dbo].[SophosConfigDealType] (
    [SophosConfigDealTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (100) NULL,
    [Status]                 INT            NULL,
    [LastUpdated]            DATETIME       CONSTRAINT [DF_SophosConfigDealType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]            DATETIME       CONSTRAINT [DF_SophosConfigDealType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SophosConfigDealType] PRIMARY KEY CLUSTERED ([SophosConfigDealTypeId] ASC)
);

