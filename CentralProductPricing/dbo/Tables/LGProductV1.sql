﻿CREATE TABLE [dbo].[LGProductV1] (
    [LGProductId]      INT             IDENTITY (1, 1) NOT NULL,
    [LGConfigSegId]    INT             NULL,
    [LGConfigSeriesId] INT             NULL,
    [LGConfigInchId]   INT             NULL,
    [Model]            NVARCHAR (20)   NULL,
    [Description]      NVARCHAR (MAX)  NULL,
    [Price]            DECIMAL (10, 2) NULL,
    [Status]           INT             NULL,
    [LastUpdated]      DATETIME        CONSTRAINT [DF_LGProduct_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]      DATETIME        CONSTRAINT [DF_LGProduct_DateCreated] DEFAULT (getdate()) NULL
);

