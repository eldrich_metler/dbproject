﻿CREATE TABLE [dbo].[SophosProductHistoryV1] (
    [SophosProductHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [SophosProductId]        INT             NULL,
    [UserAccountId]          INT             NULL,
    [SophosProductBandId]    INT             NULL,
    [Price]                  DECIMAL (10, 2) NULL,
    [DateCreated]            DATETIME        NULL,
    [LastUpdated]            DATETIME        NULL
);

