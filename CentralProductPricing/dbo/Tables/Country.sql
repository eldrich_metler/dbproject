﻿CREATE TABLE [dbo].[Country] (
    [CountryId]    INT            IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (250) NULL,
    [Status]       INT            NULL,
    [DateCreated]  DATETIME       NULL,
    [DateModified] DATETIME       NULL,
    [DateDeleted]  DATETIME       NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryId] ASC)
);

