﻿CREATE TABLE [dbo].[AutodeskProductHistory] (
    [AutodeskProductHistoryId]          INT             IDENTITY (1, 1) NOT NULL,
    [AutodeskProductId]                 INT             NULL,
    [UserAccountId]                     INT             NULL,
    [SRP]                               DECIMAL (12, 2) NULL,
    [DistributorIncrementalDiscountPercent] DECIMAL (4, 2)  NULL,
    [DistributorBaseDiscountPercent]    DECIMAL (4, 2)  NULL,
    [NetCost]                       DECIMAL (12, 2) NULL,
    [DateCreated]                       DATETIME        NULL,
    [LastUpdated]                       DATETIME        NULL,
    CONSTRAINT [PK_AutoDeskProductHistory] PRIMARY KEY CLUSTERED ([AutodeskProductHistoryId] ASC),
    CONSTRAINT [FK_AutoDeskProductHistory_AutoDeskProduct] FOREIGN KEY ([AutodeskProductId]) REFERENCES [dbo].[AutodeskProduct] ([AutodeskProductId]),
    CONSTRAINT [FK_AutodeskProductHistory_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

