﻿CREATE TABLE [dbo].[ConfigPlugin] (
    [ConfigPluginId]     INT           IDENTITY (1, 1) NOT NULL,
    [SystemName]         VARCHAR (100) NOT NULL,
    [FriendlyName]       VARCHAR (100) NOT NULL,
    [Version]            VARCHAR (20)  NOT NULL,
    [ControllerName]     VARCHAR (50)  NOT NULL,
    [DefaultAction]      VARCHAR (50)  NOT NULL,
    [ConfigPluginTypeId] INT           NULL,
    [BrandId]            INT           NULL,
    [DateCreated]        DATETIME      NOT NULL,
    [LastUpdated]        DATETIME      NOT NULL,
    [Description]        VARCHAR (MAX) NULL,
    [Status]             INT           CONSTRAINT [DF_ConfigPlugin_Status] DEFAULT ((1)) NULL,
    [ReleaseNotes]       VARCHAR (MAX) NULL,
    CONSTRAINT [PK_ConfigPlugin] PRIMARY KEY CLUSTERED ([ConfigPluginId] ASC),
    CONSTRAINT [FK_ConfigPlugin_Brand] FOREIGN KEY ([BrandId]) REFERENCES [dbo].[Brand] ([BrandId]),
    CONSTRAINT [FK_ConfigPluginType_ConfigPlugin] FOREIGN KEY ([ConfigPluginTypeId]) REFERENCES [dbo].[ConfigPluginType] ([ConfigPluginTypeId])
);

