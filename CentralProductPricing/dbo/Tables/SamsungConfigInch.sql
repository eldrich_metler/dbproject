﻿CREATE TABLE [dbo].[SamsungConfigInch] (
    [SamsungConfigInchId] INT           IDENTITY (1, 1) NOT NULL,
    [Inch]                NVARCHAR (50) NULL,
    [Status]              INT           NULL,
    [LastUpdated]         DATETIME      CONSTRAINT [DF_SamsungConfigInch_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]         DATETIME      CONSTRAINT [DF_SamsungConfigInch_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_SamsungConfigInch] PRIMARY KEY CLUSTERED ([SamsungConfigInchId] ASC)
);

