﻿CREATE TABLE [dbo].[VeritasConfigFulfillmentType] (
    [VeritasConfigFulfillmentTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                           NVARCHAR (100) NULL,
    [Status]                         INT            NULL,
    [LastUpdated]                    DATETIME       CONSTRAINT [DF_VeritasConfigFulfillmentType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]                    DATETIME       CONSTRAINT [DF_VeritasConfigFulfillmentType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigFulfillmentType] PRIMARY KEY CLUSTERED ([VeritasConfigFulfillmentTypeId] ASC)
);

