﻿CREATE TABLE [dbo].[VeritasConfigTransactionType] (
    [VeritasConfigTransactionTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]                           NVARCHAR (100) NULL,
    [Status]                         INT            NULL,
    [LastUpdated]                    DATETIME       CONSTRAINT [DF_VeritasConfigTransactionType_LastUpdated] DEFAULT (getdate()) NULL,
    [DateCreated]                    DATETIME       CONSTRAINT [DF_VeritasConfigTransactionType_DateCreated] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_VeritasConfigTransactionType] PRIMARY KEY CLUSTERED ([VeritasConfigTransactionTypeId] ASC)
);

