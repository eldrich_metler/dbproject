﻿CREATE TABLE [dbo].[ServiceBulkUploadItem] (
    [ServiceBulkUploadItemId] INT            IDENTITY (1, 1) NOT NULL,
    [ServiceBulkUploadId]     INT            NOT NULL,
    [ServiceCode]             NVARCHAR (MAX) NULL,
    [Name]                    NVARCHAR (250) NOT NULL,
    [Description]             NVARCHAR (MAX) NULL,
    [MoreInformation]         NVARCHAR (MAX) NULL,
    [FAQ]                     NVARCHAR (MAX) NULL,
    [SLA]                     NVARCHAR (MAX) NULL,
    [SystemRequirements]      NVARCHAR (MAX) NULL,
    [APIIntegration]          BIT            NULL,
    [ServiceGroup]            NVARCHAR (250) NULL,
    [Status]                  INT            NOT NULL,
    [DateCreated]             DATETIME       NOT NULL,
    [DateModified]            DATETIME       NOT NULL,
    [DateArchived]            DATETIME       NULL,
    CONSTRAINT [PK_ServiceBulkUploadItem] PRIMARY KEY CLUSTERED ([ServiceBulkUploadItemId] ASC),
    CONSTRAINT [FK_ServiceBulkUploadItem_ServiceBulkUpload] FOREIGN KEY ([ServiceBulkUploadId]) REFERENCES [dbo].[ServiceBulkUpload] ([ServiceBulkUploadId])
);





