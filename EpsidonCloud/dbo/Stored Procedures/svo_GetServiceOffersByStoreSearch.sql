﻿

CREATE PROCEDURE [dbo].[svo_GetServiceOffersByStoreSearch]
	@StoreId int,
	@CustomerId int = null,
	@Search varchar(256),	
	@Category varchar(256),		
	@NumRowsToReturn int,
	@NumRowsToSkip int,
	@Sort varchar(64),
	@TotalRows int output
AS
BEGIN
	set nocount on;

	set @Sort = lower(@Sort)

	select @TotalRows = count(c.ServiceId)
	from (
			select s.ServiceId
				from StagingStoreService s
					inner join ServiceOffer o on o.ServiceId = s.ServiceId
					inner join Service l on l.ServiceId = o.ServiceId
					inner join MarketplaceCategoryService cs on cs.ServiceId = o.ServiceId
					inner join MarketplaceCategory c on c.MarketplaceCategoryId = cs.MarketplaceCategoryId
					inner join Provider p on p.ProviderId = s.ProviderId
				where s.StoreId = @StoreId and CustomerId is null and
					(@Search is null or @Search = '' or 
						lower(s.ServiceName) like '%' + lower(@Search) + '%' or
						lower(l.Description) like  '%' + lower(@Search) + '%'  or
						lower(p.Name) like '%' + lower(@Search) + '%' or
						lower(o.SKU) like '%' + lower(@Search) + '%' or
						lower(o.Name) like '%' + lower(@Search) + '%' 
					) and
					(@Category is null or @Category = '' or lower(c.Name) = lower(@Category))
				group by s.ServiceId
			) c ;	

	if @TotalRows is null set @TotalRows = 0;

	with StagServices(ServiceId, ServiceName, MinOfferPrice)
	as(
		select ServiceId, ServiceName, MinOfferPrice
		from (
			select distinct
				s.ServiceId, s.ServiceName, min(ServiceOfferPrice) as MinOfferPrice
			from 
				StagingStoreService s
				inner join ServiceOffer o on o.ServiceId = s.ServiceId
				inner join Service l on l.ServiceId = o.ServiceId
				inner join MarketplaceCategoryService cs on cs.ServiceId = o.ServiceId
				inner join MarketplaceCategory c on c.MarketplaceCategoryId = cs.MarketplaceCategoryId
				inner join Provider p on p.ProviderId = s.ProviderId
			where s.StoreId = @StoreId and CustomerId is null and
				(@Search is null or @Search = '' or 
					lower(s.ServiceName) like '%' + lower(@Search) + '%' or
					lower(l.Description) like  '%' + lower(@Search) + '%'  or
					lower(p.Name) like '%' + lower(@Search) + '%' or
					lower(o.SKU) like '%' + lower(@Search) + '%' or
					lower(o.Name) like '%' + lower(@Search) + '%' 
				) and
				(@Category is null or @Category = '' or lower(c.Name) = lower(@Category))
			group by 
				s.ServiceId, s.ServiceName
			) x
		order by
			case @Sort when 'desc' then ServiceName end desc,
			case @Sort when 'price_desc' then MinOfferPrice end desc,
			case @Sort when 'asc' then ServiceName end asc,
			case @Sort when 'price_asc' then MinOfferPrice end asc
		offset @NumRowsToSkip rows
		fetch next @NumRowsToReturn rows only
	)

	select e.ServiceId, e.ServiceName, v.Description as ServiceDescription, 
		p.Name as ServiceProvider, p.ProviderId as ServiceProviderId, o.Name as OfferName, o.ServiceOfferId,
		o.Description as OfferDescription, o.MinimumQuantity, o.SKU,
		case when cup.ServiceOfferPrice is not null and cup.ServiceOfferPrice <> 0 then cup.ServiceOfferPrice 
			when cus.ServiceOfferPrice is not null and cus.ServiceOfferPrice <> 0 then cus.ServiceOfferPrice 
			when prov.ServiceOfferPrice is not null and prov.ServiceOfferPrice <> 0 then prov.ServiceOfferPrice 
			else e.ServiceOfferPrice end ServiceOfferPrice, 	
		case when cup.ServiceOfferVatPrice is not null and cup.ServiceOfferVatPrice <> 0 then cup.ServiceOfferVatPrice 
			when cus.ServiceOfferVatPrice is not null and cus.ServiceOfferVatPrice <> 0 then cus.ServiceOfferVatPrice 
			when prov.ServiceOfferVatPrice is not null and prov.ServiceOfferVatPrice <> 0 then prov.ServiceOfferVatPrice
			else e.ServiceOfferVatPrice end ServiceOfferVatPrice,
		f.FileId, o.PricingBillingCycle, v.DateCreated, v.LastUpdated		
	from StagServices s
	inner join Service v on v.ServiceId = s.ServiceId
	inner join StagingStoreService e on e.ServiceId = v.ServiceId 
		and e.StoreId = @StoreId and e.CustomerId is null
	inner join ServiceOffer o on o.ServiceOfferId = e.ServiceOfferId
	inner join Provider p on p.ProviderId = v.ProviderId
	outer apply(
		select top 1 fs.FileId 
		from FileService fs
		inner join [File] fi on fi.FileId = fs.FileId and fi.Status = 1
		where fs.ServiceId = v.ServiceId
	) f
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice
		from StagingStoreService 
		where CustomerId = @CustomerId and CustomerId is not null
		and StoreId = @StoreId and CustomerProviderId is null
	) cus on cus.ServiceOfferId = e.ServiceOfferId
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, CustomerProviderId
		from StagingStoreService 
		where CustomerId = @CustomerId and CustomerId is not null
		and StoreId = @StoreId and CustomerProviderId is not null
	) cup on cup.ServiceOfferId = e.ServiceOfferId and cup.CustomerProviderId = e.ProviderId
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, ProviderId
		from StagingStoreService 
		where StoreId = @StoreId and IsProviderMarginPrice = 1
	) prov on prov.ServiceOfferId = e.ServiceOfferId and prov.ProviderId = e.ProviderId
	order by
		case @Sort when 'desc' then e.ServiceName end desc,
		case @Sort when 'price_desc' then MinOfferPrice end desc,
		case @Sort when 'asc' then e.ServiceName end asc,
		case @Sort when 'price_asc' then MinOfferPrice end asc

END