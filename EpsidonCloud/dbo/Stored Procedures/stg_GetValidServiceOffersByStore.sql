﻿

CREATE PROCEDURE [dbo].[stg_GetValidServiceOffersByStore]
	@StoreId int,
	@StoreTradeCurrencyId int
AS
BEGIN
	set nocount on;  

	declare @ROETable table (CompanyId int, CurrencyFromId int, CurrencyToId int, Rate decimal(18,6), IsParentCompanyROE bit)

	create table #ServiceOfferTable (StoreId int, ServiceId int, ServiceOfferId int, ProviderId int, 
			CompanyVendorPrice decimal(18, 2), CompanyOfferPrice decimal(18, 2), CompanyOfferMargin decimal(18, 2), CompanyResellerPrice decimal(18, 2),
			SubCompanyVendorPrice decimal(18, 2), SubCompanyOfferPrice decimal(18, 2), SubCompanyOfferMargin decimal(18, 2), SubCompanyResellerPrice decimal(18, 2),
			StoreVendorPrice decimal(18, 2), StoreOfferPrice decimal(18, 2), StoreOfferMargin decimal(18, 2),
			StorePriceLockDuration int, StorePriceLockPrice decimal(18, 2), StoreVAT decimal(18, 2), StoreTradeConfigCurrencyId int,
			StoreEndUserConfigCurrencyId int, CompanyRoeRate decimal(18, 2), StoreRoeRate decimal(18, 2), VendorConfigCurrencyId int,
			ServiceName nvarchar(250), ServiceOfferPrice decimal(18,2), ServiceOfferVatPrice decimal(18,2), IsProviderMarginPrice bit)

	-- insert company ROE Meh
	insert into @ROETable
	select s.CompanyId, r.CurrencyFromId, r.CurrencyToId, r.Rate, 0	
	from Store s 
	left join CompanyROE r on r.CompanyId = s.CompanyId
	where
		s.StoreId = @StoreId and r.Rate is not null

	-- insert parent company ROE
	insert into @ROETable
	select c.CompanyId,  r.CurrencyFromId, r.CurrencyToId, r.Rate, 1	
	from Store s
	inner join Company c on c.CompanyId = s.CompanyId and s.StoreId = @StoreId
	left join CompanyROE r on r.CompanyId = c.ParentCompanyId
	where
		s.StoreId = @StoreId and r.Rate is not null;

	with StorePricing
	(
		ServiceOfferPricingId, ServiceOfferId, ConfigCurrencyId, VendorPrice,
		OfferPrice, OfferMargin, PriceLockDuration, PriceLockPrice, Amount, 
		ServiceType, VAT, TaxType, StoreROE, StoreId, ProviderConfigCurrencyId,
		ProviderId, ServiceId, ServiceName
	)
	as
	(
		select p.ServiceOfferPricingId, p.ServiceOfferId, p.ConfigCurrencyId, p.VendorPrice,
			p.OfferPrice, p.OfferMargin, p.PriceLockDuration, p.PriceLockPrice, p.Amount, 
			s.Type, h.VAT, h.TaxType, m.Rate, h.StoreId, cc.ConfigCurrencyId, 
			s.ProviderId, s.ServiceId, s.Name
		from Store h
		inner join StoreService t on t.StoreId = h.StoreId 
			and h.StoreId = @StoreId and t.StoreId = @StoreId
		inner join Service s on s.ServiceId = t.ServiceId and s.[Status] = 1 
		inner join ServiceOffer o on o.ServiceId = s.ServiceId and o.[Status] = 1
		inner join MarketplaceCategoryService mcs on mcs.ServiceId = s.ServiceId
		inner join ServiceOfferPricing p on p.ServiceOfferId = o.ServiceOfferId 
			and p.Status = 1 and p.StoreId = @StoreId and IsDisplay = 1	
		inner join Provider pr on pr.ProviderId = s.ProviderId and pr.[Status] = 1
		inner join ProviderConfigCurrency pc on pc.ProviderId = pr.ProviderId
		inner join ConfigCurrency cc on cc.ConfigCurrencyId = pc.ConfigCurrencyId	
		inner join StoreConfigCurrency sc on sc.StoreId = h.StoreId		
		left join StoreROE m on m.StoreId = @StoreId 
			and m.CurrencyFromId = @StoreTradeCurrencyId and m.CurrencyToId = sc.ConfigCurrencyId					
	), 
	CompanyPricing 
	(
		ServiceOfferId, VendorPrice, OfferMargin, OfferPrice, CompanyMarginPrice, CompanyResellerPrice
	)
	as
	(
		select p.ServiceOfferId, VendorPrice, OfferMargin, OfferPrice,
		case when OfferMargin <> 0 then dbo.udf_CalcMarginValue(VendorPrice,OfferMargin) else 0 end CompanyMarginPrice,
		case 
			when OfferMargin is not null and OfferMargin <> 0 then VendorPrice + dbo.udf_CalcMarginValue(VendorPrice,OfferMargin)
			when OfferPrice is not null and OfferPrice <> 0 and OfferPrice < VendorPrice then 0 
			when OfferPrice is not null and OfferPrice <> 0 then OfferPrice
			when VendorPrice is not null and VendorPrice <> 0 then VendorPrice
			else 0 		
		end CompanyResellerPrice
		from StoreService t
		inner join Service s on s.ServiceId = t.ServiceId and t.StoreId = @StoreId and s.[Status] = 1 
		inner join ServiceOffer o on o.ServiceId = s.ServiceId and o.[Status] = 1
		inner join ServiceOfferPricing p on p.ServiceOfferId = o.ServiceOfferId 
			and p.Status = 1 and p.StoreId is null and p.CompanyId is null
	),
	SubCompanyPricing
	(
		ServiceOfferId, VendorPrice, OfferMargin, OfferPrice, SetOwnPrice, SubCompanyMarginPrice, SubCompanyResellerPrice
	)
	as
	(
		select p.ServiceOfferId, VendorPrice, OfferMargin, OfferPrice, c.SetOwnPrice,
		case when OfferMargin <> 0 then dbo.udf_CalcMarginValue(VendorPrice,OfferMargin) else 0 end SubCompanyMarginPrice,
		case 
			when OfferMargin is not null and OfferMargin <> 0 then VendorPrice + dbo.udf_CalcMarginValue(VendorPrice,OfferMargin)
			when OfferPrice is not null and OfferPrice <> 0 and OfferPrice < VendorPrice then 0 
			when OfferPrice is not null and OfferPrice <> 0 then OfferPrice
			when VendorPrice is not null and VendorPrice <> 0 then VendorPrice
			else 0 		
		end SubCompanyResellerPrice
		from Store t
		inner join CompanyService c on c.CompanyId = t.CompanyId and t.StoreId = @StoreId
		inner join Service s on s.ServiceId = c.ServiceId and s.[Status] = 1 
		inner join ServiceOffer o on o.ServiceId = s.ServiceId and o.[Status] = 1 
		inner join ServiceOfferPricing p on p.ServiceOfferId = o.ServiceOfferId 
			and p.Status = 1 and p.StoreId is null and p.CompanyId = t.CompanyId	
	)

	insert into #ServiceOfferTable 
	select distinct @StoreId, s.ServiceId, s.ServiceOfferId, s.ProviderId, y.VendorPrice, y.OfferPrice, y.OfferMargin, y.CompanyResellerPrice,
		u.VendorPrice, u.OfferPrice, u.OfferMargin, u.SubCompanyResellerPrice, s.VendorPrice, s.OfferPrice, s.OfferMargin, 
		PriceLockDuration, PriceLockPrice, VAT, @StoreTradeCurrencyId, m.ConfigCurrencyId,
		case when k.Rate is null then k.Rate else d.Rate end, s.StoreROE, ProviderConfigCurrencyId, 
		s.ServiceName,
		dbo.udf_CalcServiceOfferPrice(
			ServiceOfferPricingId,
			case when u.ServiceOfferId is null then 0 else 1 end,
			u.SubCompanyResellerPrice,
			u.SetOwnPrice,
			ProviderConfigCurrencyId,
			@StoreTradeCurrencyId,
			m.ConfigCurrencyId,
			k.Rate,
			d.Rate,
			y.CompanyResellerPrice,
			s.StoreROE,
			case 
				when pmy.Margin is not null and pmy.Margin <> 0 then pmy.Margin
				when s.OfferMargin is not null and s.OfferMargin <> 0 then s.OfferMargin
				when smy.Margin is not null and smy.Margin <> 0 then smy.Margin			
				else 0
			end,			
			s.OfferPrice,
			s.VendorPrice,
			s.VAT,
			s.TaxType,
			s.PriceLockDuration,
			s.PriceLockPrice,
			s.ServiceType
		) as ServiceOfferPrice,
		dbo.udf_CalcServiceOfferVatPrice(
			ServiceOfferPricingId,
			case when u.ServiceOfferId is null then 0 else 1 end,
			u.SubCompanyResellerPrice,
			u.SetOwnPrice,
			ProviderConfigCurrencyId,
			@StoreTradeCurrencyId,
			m.ConfigCurrencyId,
			k.Rate,
			d.Rate,
			y.CompanyResellerPrice,
			s.StoreROE,
			case 
				when pmy.Margin is not null and pmy.Margin <> 0 then pmy.Margin
				when s.OfferMargin is not null and s.OfferMargin <> 0 then s.OfferMargin
				when smy.Margin is not null and smy.Margin <> 0 then smy.Margin			
				else 0
			end,			
			s.OfferPrice,
			s.VendorPrice,
			s.VAT,
			s.TaxType,
			s.PriceLockDuration,
			s.PriceLockPrice,
			s.ServiceType
		) as ServiceOfferVatPrice,
		case 
			when pmy.Margin is not null and pmy.Margin <> 0 then 1
			else 0
		end
	from StorePricing s
	inner join (
		select top 1 * 
		from StoreConfigCurrency
		where StoreId = @StoreId
	) m on m.StoreId = s.StoreId
	left join @ROETable k on k.CurrencyFromId = ProviderConfigCurrencyId 
		and k.CurrencyToId = @StoreTradeCurrencyId and k.IsParentCompanyROE = 0
	left join @ROETable d on d.CurrencyFromId = ProviderConfigCurrencyId
		and d.CurrencyToId = @StoreTradeCurrencyId and d.IsParentCompanyROE = 1
	left join CompanyPricing y on y.ServiceOfferId = s.ServiceOfferId
	left join SubCompanyPricing u on u.ServiceOfferId = s.ServiceOfferId
	left join (
		select top 1 StoreId, sm.Margin 
		from StoreMarginPolicy smp
		inner join MarginPolicy sm on sm.MarginPolicyId = smp.MarginPolicyId
			and StoreId = @StoreId and sm.Status = 1 
			and sm.Margin is not null 
			--and sm.Margin > 0
	) smy on smy.StoreId = m.StoreId
	outer apply (
		select top 1 pm.Margin 
		from ProviderMarginPolicy pmp 
		inner join MarginPolicy pm on pm.MarginPolicyId = pmp.MarginPolicyId 
			and StoreId = @StoreId and pmp.ProviderId = s.ProviderId and pm.Status = 1
			and pm.Margin is not null 
			--and pm.Margin > 0
	) pmy
	option (OPTIMIZE FOR (@StoreId UNKNOWN, @StoreTradeCurrencyId UNKNOWN))

	select 0 as StagingStoreServiceId,  
		StoreId, ServiceId, ServiceOfferId, ProviderId, null as CustomerId, null as CustomerProviderId,
		CompanyVendorPrice , CompanyOfferPrice , CompanyOfferMargin, CompanyResellerPrice,
		SubCompanyVendorPrice , SubCompanyOfferPrice , SubCompanyOfferMargin, SubCompanyResellerPrice,
		StoreVendorPrice , StoreOfferPrice , StoreOfferMargin ,
		StorePriceLockDuration, StorePriceLockPrice , StoreVAT , StoreTradeConfigCurrencyId,
		StoreEndUserConfigCurrencyId, CompanyRoeRate , StoreRoeRate , VendorConfigCurrencyId, 
		ServiceName, ServiceOfferPrice, ServiceOfferVatPrice, IsProviderMarginPrice, GetDate() as DateCreated
	from 
		#ServiceOfferTable 
	where 
		ServiceOfferPrice is not null and ServiceOfferPrice > 0

	drop table #ServiceOfferTable
END

