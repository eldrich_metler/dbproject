﻿

CREATE PROCEDURE [dbo].[svo_GetServiceOffersByServiceTextLookup]
	@StoreId int,
	@CustomerId int = null,
	@ServiceLookup dbo.udtServiceTextLookupType readonly
AS
BEGIN
	set nocount on;

	with StagLookupServices(ServiceId, ServiceName, LookupReference, MinOfferPrice)
	as(
		select ServiceId, ServiceName, Reference, MinOfferPrice
		from (
			select distinct
				s.ServiceId, s.ServiceName, l.Reference, min(ServiceOfferPrice) as MinOfferPrice
			from 
				StagingStoreService s
				inner join @ServiceLookup l on lower(l.ServiceName) = lower(s.ServiceName)
					and l.Reference = cast(s.ServiceId as varchar(128)) and s.StoreId = @StoreId
			group by 
				s.ServiceId, s.ServiceName, l.Reference
		) x
	),
	StagLookupOffers(ServiceId, ServiceName, LookupReference, MinOfferPrice)
		as(
			select ServiceId, ServiceName, Reference, MinOfferPrice
			from (
				select distinct
					s.ServiceId, s.ServiceName, l.Reference, min(ServiceOfferPrice) as MinOfferPrice
				from 
					StagingStoreService s
					inner join ServiceOffer o on o.ServiceOfferId = s.ServiceOfferId 
						and s.StoreId = @StoreId
					inner join @ServiceLookup l on lower(l.ServiceName) = lower(o.Name) and
					(
						l.Reference = cast(o.ServiceOfferId as varchar(128))
						or l.Reference = cast(o.ServiceId as varchar(128))
						or l.Reference = cast(o.SKU as varchar(128))
					)
				group by 
					s.ServiceId, s.ServiceName, l.Reference
			) x
		)

	select distinct * from (
		select e.ServiceId, e.ServiceName, v.Description as ServiceDescription, 
			p.Name as ServiceProvider, p.ProviderId as ServiceProviderId, o.Name as OfferName, o.ServiceOfferId,
			o.Description as OfferDescription, o.MinimumQuantity, o.SKU, 
			case when cup.ServiceOfferPrice is not null and cup.ServiceOfferPrice <> 0 then cup.ServiceOfferPrice 
				when cus.ServiceOfferPrice is not null and cus.ServiceOfferPrice <> 0 then cus.ServiceOfferPrice 
				when prov.ServiceOfferPrice is not null and prov.ServiceOfferPrice <> 0 then prov.ServiceOfferPrice 
				else e.ServiceOfferPrice end ServiceOfferPrice, 	
			case when cup.ServiceOfferVatPrice is not null and cup.ServiceOfferVatPrice <> 0 then cup.ServiceOfferVatPrice 
				when cus.ServiceOfferVatPrice is not null and cus.ServiceOfferVatPrice <> 0 then cus.ServiceOfferVatPrice 
				when prov.ServiceOfferVatPrice is not null and prov.ServiceOfferVatPrice <> 0 then prov.ServiceOfferVatPrice 
				else e.ServiceOfferVatPrice end ServiceOfferVatPrice,
			f.FileId, o.PricingBillingCycle, v.DateCreated, v.LastUpdated		
		from StagLookupServices s
		inner join Service v on v.ServiceId = s.ServiceId
		inner join StagingStoreService e on e.ServiceId = v.ServiceId 
			and e.StoreId = @StoreId and e.CustomerId is null
		inner join ServiceOffer o on o.ServiceOfferId = e.ServiceOfferId
		inner join Provider p on p.ProviderId = v.ProviderId
		outer apply(
			select top 1 fs.FileId 
			from FileService fs
			inner join [File] fi on fi.FileId = fs.FileId and fi.Status = 1
			where fs.ServiceId = v.ServiceId
		) f
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice
			from StagingStoreService 
			where CustomerId = @CustomerId and CustomerId is not null
			and StoreId = @StoreId and CustomerProviderId is null
		) cus on cus.ServiceOfferId = e.ServiceOfferId
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, CustomerProviderId
			from StagingStoreService 
			where CustomerId = @CustomerId and CustomerId is not null
			and StoreId = @StoreId and CustomerProviderId is not null
		) cup on cup.ServiceOfferId = e.ServiceOfferId and cup.CustomerProviderId = e.ProviderId
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, ProviderId
			from StagingStoreService 
			where StoreId = @StoreId and IsProviderMarginPrice = 1
		) prov on prov.ServiceOfferId = e.ServiceOfferId and prov.ProviderId = e.ProviderId
		where
			s.LookupReference is not null

		union all

		select e.ServiceId, e.ServiceName, v.Description as ServiceDescription, 
			p.Name as ServiceProvider, p.ProviderId as ServiceProviderId, o.Name as OfferName, o.ServiceOfferId,
			o.Description as OfferDescription, o.MinimumQuantity, o.SKU, 
			case when cup.ServiceOfferPrice is not null and cup.ServiceOfferPrice <> 0 then cup.ServiceOfferPrice 
				when cus.ServiceOfferPrice is not null and cus.ServiceOfferPrice <> 0 then cus.ServiceOfferPrice 
				when prov.ServiceOfferPrice is not null and prov.ServiceOfferPrice <> 0 then prov.ServiceOfferPrice 
				else e.ServiceOfferPrice end ServiceOfferPrice, 	
			case when cup.ServiceOfferVatPrice is not null and cup.ServiceOfferVatPrice <> 0 then cup.ServiceOfferVatPrice 
				when cus.ServiceOfferVatPrice is not null and cus.ServiceOfferVatPrice <> 0 then cus.ServiceOfferVatPrice 
				when prov.ServiceOfferVatPrice is not null and prov.ServiceOfferVatPrice <> 0 then prov.ServiceOfferVatPrice 
				else e.ServiceOfferVatPrice end ServiceOfferVatPrice,
			f.FileId, o.PricingBillingCycle, v.DateCreated, v.LastUpdated		
		from StagLookupOffers s
		inner join Service v on v.ServiceId = s.ServiceId
		inner join StagingStoreService e on e.ServiceId = v.ServiceId 
			and e.StoreId = @StoreId and e.CustomerId is null
		inner join ServiceOffer o on o.ServiceOfferId = e.ServiceOfferId
		inner join Provider p on p.ProviderId = v.ProviderId
		outer apply(
			select top 1 FileId 
			from FileService
			where ServiceId = v.ServiceId
		) f
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice
			from StagingStoreService 
			where CustomerId = @CustomerId and CustomerId is not null
			and StoreId = @StoreId and CustomerProviderId is null
		) cus on cus.ServiceOfferId = e.ServiceOfferId
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, CustomerProviderId
			from StagingStoreService 
			where CustomerId = @CustomerId and CustomerId is not null
			and StoreId = @StoreId and CustomerProviderId is not null
		) cup on cup.ServiceOfferId = e.ServiceOfferId and cup.CustomerProviderId = e.ProviderId
		left join (
			select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, ProviderId
			from StagingStoreService 
			where StoreId = @StoreId and IsProviderMarginPrice = 1
		) prov on prov.ServiceOfferId = e.ServiceOfferId and prov.ProviderId = e.ProviderId
		where
			s.LookupReference is not null
	) f
END