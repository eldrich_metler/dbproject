﻿

CREATE PROCEDURE [dbo].[svo_GetServiceOffersByStoreCategory]
	@StoreId int,
	@CustomerId int = null,
	@CategoryId int,
	@UseOwnCatStructure bit,
	@NumRowsToReturn int,
	@NumRowsToSkip int,
	@Sort varchar(64),
	@TotalRows int output
AS
BEGIN
	set nocount on;

	set @Sort = lower(@Sort)

	declare @CompanyId int = (select top 1 CompanyId from Store where StoreId = @StoreId);

	create table #ServiceOffers (ServiceId int, ServiceName varchar(256), ServiceDescription nvarchar(max), 
		ServiceProvider varchar(256), ServiceProviderId int, OfferName varchar(256), ServiceOfferId int,
		OfferDescription  nvarchar(max), MinimumQuantity int, SKU  varchar(256), ServiceOfferPrice decimal (18,2), 
		ServiceOfferVatPrice decimal (18,2), FileId int, PricingBillingCycle int, DateCreated datetime, LastUpdated datetime, TotalRows int)

	set @TotalRows = 0;

	with Descendants(CategoryId) 
	as
	(
	  select @CategoryId

	  union all

	  select e.MarketplaceCategoryId
	  from MarketplaceCategory e
	  where
		[Status] = 1 and
		ParentMarketplaceCategoryId = @CategoryId and
		(
			(@UseOwnCatStructure = 1 and StoreId = @StoreId) or
			(@UseOwnCatStructure = 0 and StoreId is null and CompanyId = @CompanyId) 
		)
	)
	,
	SubDescendants(CategoryId) 
	as
	(
	  select e.MarketplaceCategoryId
	  from MarketplaceCategory e
		inner join Descendants d on e.MarketplaceCategoryId = d.CategoryId	
		and [Status] = 1 

	  union all

	  select e.MarketplaceCategoryId
	  from MarketplaceCategory e
		inner join SubDescendants t on t.CategoryId = e.ParentMarketplaceCategoryId 
		and [Status] = 1
	  where		
		(@UseOwnCatStructure = 1 and StoreId = @StoreId) or
		(@UseOwnCatStructure = 0 and StoreId is null and CompanyId = @CompanyId) 		
	),
	StagServices(ServiceId, ServiceName, MinOfferPrice, Total)
	as(
		select ServiceId, ServiceName, MinOfferPrice, Total
		from (
		select distinct
			s.ServiceId, s.ServiceName, min(ServiceOfferPrice) as MinOfferPrice, count(*) over() Total
		from 
			StagingStoreService s
			inner join ServiceOffer o on o.ServiceId = s.ServiceId
			inner join MarketplaceCategoryService cs on cs.ServiceId = o.ServiceId
			inner join MarketplaceCategory c on c.MarketplaceCategoryId = cs.MarketplaceCategoryId
			inner join (select distinct CategoryId from SubDescendants) d on d.CategoryId = c.MarketplaceCategoryId
		where 
			s.StoreId = @StoreId and CustomerId is null
		group by 
			s.ServiceId, s.ServiceName
		) x
		order by
			case @Sort when 'desc' then ServiceName end desc,
			case @Sort when 'price_desc' then MinOfferPrice end desc,
			case @Sort when 'asc' then ServiceName end asc,
			case @Sort when 'price_asc' then MinOfferPrice end asc
		offset @NumRowsToSkip rows
		fetch next @NumRowsToReturn rows only
	)

	insert into #ServiceOffers
	select e.ServiceId, e.ServiceName, v.Description as ServiceDescription, 
		p.Name as ServiceProvider, p.ProviderId as ServiceProviderId, o.Name as OfferName, o.ServiceOfferId,
		o.Description as OfferDescription, o.MinimumQuantity, o.SKU, e.ServiceOfferPrice, 
		e.ServiceOfferVatPrice, f.FileId, o.PricingBillingCycle, v.DateCreated, v.LastUpdated, s.Total	
	from StagServices s
	inner join Service v on v.ServiceId = s.ServiceId
	inner join StagingStoreService e on e.ServiceId = v.ServiceId 
		and e.StoreId = @StoreId and e.CustomerId is null
	inner join ServiceOffer o on o.ServiceOfferId = e.ServiceOfferId
	inner join Provider p on p.ProviderId = v.ProviderId
	outer apply(
		select top 1 fs.FileId 
		from FileService fs
		inner join [File] fi on fi.FileId = fs.FileId and fi.Status = 1
		where fs.ServiceId = v.ServiceId
	) f

	set @TotalRows = (select top 1 TotalRows from #ServiceOffers)
	if @TotalRows is null set @TotalRows = 0

	select ServiceId, ServiceName, ServiceDescription, 
	    ServiceProvider, ServiceProviderId, OfferName, s.ServiceOfferId,
		OfferDescription, MinimumQuantity, SKU, 
		case when cup.ServiceOfferPrice is not null and cup.ServiceOfferPrice <> 0 then cup.ServiceOfferPrice 
			when cus.ServiceOfferPrice is not null and cus.ServiceOfferPrice <> 0 then cus.ServiceOfferPrice 
			when prov.ServiceOfferPrice is not null and prov.ServiceOfferPrice <> 0 then prov.ServiceOfferPrice 
			else s.ServiceOfferPrice end ServiceOfferPrice, 
		case when cup.ServiceOfferVatPrice is not null and cup.ServiceOfferVatPrice <> 0 then cup.ServiceOfferVatPrice 
			when cus.ServiceOfferVatPrice is not null and cus.ServiceOfferVatPrice <> 0 then cus.ServiceOfferVatPrice 
			when prov.ServiceOfferVatPrice is not null and prov.ServiceOfferVatPrice <> 0 then prov.ServiceOfferVatPrice 
			else s.ServiceOfferVatPrice end ServiceOfferVatPrice,
		FileId, PricingBillingCycle, DateCreated, LastUpdated
	from #ServiceOffers s
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice
		from StagingStoreService 
		where CustomerId = @CustomerId and CustomerId is not null
		and StoreId = @StoreId and CustomerProviderId is null
	) cus on cus.ServiceOfferId = s.ServiceOfferId
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, CustomerProviderId
		from StagingStoreService 
		where CustomerId = @CustomerId and CustomerId is not null
		and StoreId = @StoreId and CustomerProviderId is not null
	) cup on cup.ServiceOfferId = s.ServiceOfferId and cup.CustomerProviderId = s.ServiceProviderId
	left join (
		select ServiceOfferId, ServiceOfferPrice, ServiceOfferVatPrice, ProviderId
		from StagingStoreService 
		where StoreId = @StoreId and IsProviderMarginPrice = 1
	) prov on prov.ServiceOfferId = s.ServiceOfferId and prov.ProviderId = s.ServiceProviderId
	order by
		case @Sort when 'desc' then ServiceName end desc,
		case @Sort when 'price_desc' then s.ServiceOfferPrice end desc,
		case @Sort when 'asc' then ServiceName end asc,
		case @Sort when 'price_asc' then s.ServiceOfferPrice end asc
END