﻿CREATE TABLE [dbo].[AzureCustomerMarginPolicy] (
    [MarginPolicyId] INT NOT NULL,
    [StoreId]        INT NOT NULL,
    [CustomerId]     INT NOT NULL,
    CONSTRAINT [PK_AzureCustomerMarginPolicy] PRIMARY KEY CLUSTERED ([MarginPolicyId] ASC),
    CONSTRAINT [FK_AzureCustomerMarginPolicy_MarginPolicy] FOREIGN KEY ([MarginPolicyId]) REFERENCES [dbo].[MarginPolicy] ([MarginPolicyId]),
    CONSTRAINT [FK_AzureCustomerMarginPolicy_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

