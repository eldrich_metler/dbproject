﻿CREATE TABLE [dbo].[Company] (
    [CompanyId]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]                    NVARCHAR (500) NOT NULL,
    [Description]             NVARCHAR (MAX) NULL,
    [RegistrationNumber]      NVARCHAR (100) NULL,
    [VATNumber]               NVARCHAR (100) NULL,
    [AccountNumber]           NVARCHAR (100) NOT NULL,
    [WebsiteURL]              NVARCHAR (500) NULL,
    [Logo]                    NVARCHAR (50)  NULL,
    [Status]                  INT            NULL,
    [MarketplaceCategoryJson] NVARCHAR (MAX) NULL,
    [DefaultLanguageCode]     NVARCHAR (50)  NULL,
    [LastUpdated]             DATETIME       NULL,
    [DateCreated]             DATETIME       NULL,
    [ParentCompanyId]         INT            NULL,
    CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED ([CompanyId] ASC)
);

