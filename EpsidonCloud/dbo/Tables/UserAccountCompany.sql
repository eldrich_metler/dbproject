﻿CREATE TABLE [dbo].[UserAccountCompany] (
    [UserAccountCompanyId] INT IDENTITY (1, 1) NOT NULL,
    [UserAccountId]        INT NULL,
    [CompanyId]            INT NULL,
    CONSTRAINT [PK_UserAccountCompany] PRIMARY KEY CLUSTERED ([UserAccountCompanyId] ASC),
    CONSTRAINT [FK_UserAccountCompany_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_UserAccountCompany_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

