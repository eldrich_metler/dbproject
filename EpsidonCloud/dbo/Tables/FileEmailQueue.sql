﻿CREATE TABLE [dbo].[FileEmailQueue] (
    [FileEmailQueueId] INT IDENTITY (1, 1) NOT NULL,
    [FileId]           INT NULL,
    [EmailQueueId]     INT NULL,
    CONSTRAINT [PK_FileEmailTemplate] PRIMARY KEY CLUSTERED ([FileEmailQueueId] ASC)
);

