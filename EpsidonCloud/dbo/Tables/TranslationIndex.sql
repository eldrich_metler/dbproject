﻿CREATE TABLE [dbo].[TranslationIndex] (
    [TranslationIndexId] INT            IDENTITY (1, 1) NOT NULL,
    [RecordId]           INT            NULL,
    [TableName]          NVARCHAR (500) NULL,
    [LanguageCodeId]     INT            NULL,
    [ColumnName]         NVARCHAR (500) NULL,
    [TranslationValue]   NVARCHAR (MAX) NULL,
    [DateCreated]        DATETIME       NULL,
    [LastUpdated]        DATETIME       NULL,
    CONSTRAINT [PK_TranslationIndex] PRIMARY KEY CLUSTERED ([TranslationIndexId] ASC),
    CONSTRAINT [FK_TranslationIndex_LanguageCode] FOREIGN KEY ([LanguageCodeId]) REFERENCES [dbo].[LanguageCode] ([LanguageCodeId])
);

