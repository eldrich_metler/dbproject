﻿CREATE TABLE [dbo].[OrderCreditCard] (
    [OrderCreditCardId] INT            NOT NULL,
    [PaymentGatewayId]  INT            NOT NULL,
    [PaymentToken]      NVARCHAR (256) NOT NULL,
    [CardMask]          NVARCHAR (256) NULL,
    [CardDescription]   NVARCHAR (256) NULL,
    [CardType]          NVARCHAR (256) NULL,
    [ExpirationDate]    DATETIME       NULL,
    [Reference]         NVARCHAR (256) NULL,
    [GatewayReference]  NVARCHAR (256) NULL,
    [LastUpdated]       DATETIME       NOT NULL,
    [DateCreated]       DATETIME       NOT NULL,
    CONSTRAINT [PK_OrderCreditCard] PRIMARY KEY CLUSTERED ([OrderCreditCardId] ASC),
    CONSTRAINT [FK_OrderCreditCard_Order] FOREIGN KEY ([OrderCreditCardId]) REFERENCES [dbo].[Order] ([OrderId]),
    CONSTRAINT [FK_OrderCreditCard_PaymentGateway] FOREIGN KEY ([PaymentGatewayId]) REFERENCES [dbo].[PaymentGateway] ([PaymentGatewayId])
);



