﻿CREATE TABLE [dbo].[StoreBanner] (
    [StoreBannerId] INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]       INT             NOT NULL,
    [Name]          NVARCHAR (100)  NULL,
    [ImageBinary]   VARBINARY (MAX) NULL,
    [Order]         INT             NOT NULL,
    [URL]           NVARCHAR (1000) NULL,
    [StartDate]     DATETIME        NULL,
    [EndDate]       DATETIME        NULL,
    [FileName]      NVARCHAR (50)   NULL,
    [Status]        INT             NOT NULL,
    [LastUpdated]   DATETIME        NULL,
    [DateCreated]   DATETIME        NULL,
    [ThemeType]     INT             NOT NULL
);



