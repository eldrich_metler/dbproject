﻿CREATE TABLE [dbo].[RecurringBillingSubscription] (
    [RecurringBillingSubscriptionId] INT IDENTITY (1, 1) NOT NULL,
    [RecurringBillingId]             INT NULL,
    [SubscriptionId]                 INT NULL,
    CONSTRAINT [PK_RecurringBillingSubscription] PRIMARY KEY CLUSTERED ([RecurringBillingSubscriptionId] ASC)
);

