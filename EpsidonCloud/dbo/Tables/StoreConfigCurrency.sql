﻿CREATE TABLE [dbo].[StoreConfigCurrency] (
    [StoreConfigCurrencyId] INT IDENTITY (1, 1) NOT NULL,
    [StoreId]               INT NULL,
    [ConfigCurrencyId]      INT NULL,
    CONSTRAINT [PK_StoreConfigCurrency] PRIMARY KEY CLUSTERED ([StoreConfigCurrencyId] ASC),
    CONSTRAINT [FK_StoreConfigCurrency_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId]),
    CONSTRAINT [FK_StoreConfigCurrency_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

