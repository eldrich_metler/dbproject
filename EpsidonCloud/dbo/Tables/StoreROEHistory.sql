﻿CREATE TABLE [dbo].[StoreROEHistory] (
    [StoreROEHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [StoreROEId]        INT             NULL,
    [StoreId]           INT             NULL,
    [CurrencyFromId]    INT             NULL,
    [CurrencyToId]      INT             NULL,
    [Rate]              DECIMAL (18, 4) NULL,
    [DateCreated]       DATETIME        NULL,
    [LastUpdated]       DATETIME        NULL,
    CONSTRAINT [PK_StoreROEHistory] PRIMARY KEY CLUSTERED ([StoreROEHistoryId] ASC)
);

