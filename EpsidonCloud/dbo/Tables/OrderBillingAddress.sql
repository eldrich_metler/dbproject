﻿CREATE TABLE [dbo].[OrderBillingAddress] (
    [OrderBillingAddressId] INT            NOT NULL,
    [CompanyName]           NVARCHAR (250) NULL,
    [Address]               NVARCHAR (MAX) NULL,
    [ContactNumber]         NVARCHAR (25)  NULL,
    [Country]               NVARCHAR (100) NULL,
    [City]                  NVARCHAR (100) NULL,
    [Zip]                   NVARCHAR (10)  NULL,
    [DateCreated]           DATETIME       NULL,
    [LastUpdated]           DATETIME       NULL,
    [VatNumber]             NVARCHAR (50)  NULL
);



