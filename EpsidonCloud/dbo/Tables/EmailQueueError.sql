﻿CREATE TABLE [dbo].[EmailQueueError] (
    [EmailQueueErrorId] INT            IDENTITY (1, 1) NOT NULL,
    [EmailQueueId]      INT            NULL,
    [Message]           NVARCHAR (MAX) NULL,
    [LastUpdated]       DATETIME       NULL,
    [DateCreated]       DATETIME       NULL,
    CONSTRAINT [PK_EmailQueueError] PRIMARY KEY CLUSTERED ([EmailQueueErrorId] ASC),
    CONSTRAINT [FK_EmailQueueError_EmailQueue] FOREIGN KEY ([EmailQueueId]) REFERENCES [dbo].[EmailQueue] ([EmailQueueId])
);

