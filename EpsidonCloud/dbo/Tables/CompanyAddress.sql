﻿CREATE TABLE [dbo].[CompanyAddress] (
    [CompanyAddressId] INT IDENTITY (1, 1) NOT NULL,
    [CompanyId]        INT NULL,
    [AddressId]        INT NULL,
    CONSTRAINT [PK_CompanyAddress] PRIMARY KEY CLUSTERED ([CompanyAddressId] ASC),
    CONSTRAINT [FK_CompanyAddress_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_CompanyAddress_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);

