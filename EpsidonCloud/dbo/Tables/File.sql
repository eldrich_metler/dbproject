﻿CREATE TABLE [dbo].[File] (
    [FileId]      INT             IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (100)  NULL,
    [ImageBinary] VARBINARY (MAX) NOT NULL,
    [Format]      INT             NULL,
    [Status]      INT             NULL,
    [Type]        INT             NULL,
    [Filter]      INT             NULL,
    [LastUpdated] DATETIME        NULL,
    [DateCreated] DATETIME        NULL,
    CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED ([FileId] ASC)
);



