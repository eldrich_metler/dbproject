﻿CREATE TABLE [dbo].[Cart] (
    [CartId]           INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]       INT            NULL,
    [AnonymousId]      NVARCHAR (100) NULL,
    [StoreId]          INT            NULL,
    [LanguageCodeId]   INT            NULL,
    [ConfigCurrencyId] INT            NULL,
    [CartType]         INT            NULL,
    [ExpiryDate]       DATETIME       NULL,
    [LastUpdated]      DATETIME       NULL,
    [DateCreated]      DATETIME       NULL,
    CONSTRAINT [PK_Cart] PRIMARY KEY CLUSTERED ([CartId] ASC),
    CONSTRAINT [FK_Cart_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Cart_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

