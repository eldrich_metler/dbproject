﻿CREATE TABLE [dbo].[UserAccountStore] (
    [UserAccountStoreId] INT IDENTITY (1, 1) NOT NULL,
    [UserAccountId]      INT NULL,
    [StoreId]            INT NULL,
    CONSTRAINT [PK_UserAccountStore] PRIMARY KEY CLUSTERED ([UserAccountStoreId] ASC),
    CONSTRAINT [FK_UserAccountStore_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId]),
    CONSTRAINT [FK_UserAccountStore_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

