﻿CREATE TABLE [dbo].[CustomerCreditCard] (
    [CustomerCreditCardId] INT            IDENTITY (1, 1) NOT NULL,
    [PaymentGatewayId]     INT            NOT NULL,
    [CustomerId]           INT            NOT NULL,
    [PaymentToken]         NVARCHAR (256) NOT NULL,
    [CardMask]             NVARCHAR (256) NULL,
    [CardDescription]      NVARCHAR (256) NULL,
    [CardType]             NVARCHAR (256) NULL,
    [ExpirationDate]       DATETIME       NOT NULL,
    [Reference]            NVARCHAR (256) NULL,
    [GatewayReference]     NVARCHAR (256) NULL,
    [Status]               INT            NOT NULL,
    [LastUpdated]          DATETIME       NOT NULL,
    [DateCreated]          DATETIME       NOT NULL,
    CONSTRAINT [PK_CustomerCreditCard] PRIMARY KEY CLUSTERED ([CustomerCreditCardId] ASC),
    CONSTRAINT [FK_CustomerCreditCard_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_CustomerCreditCard_PaymentGateway] FOREIGN KEY ([PaymentGatewayId]) REFERENCES [dbo].[PaymentGateway] ([PaymentGatewayId])
);



