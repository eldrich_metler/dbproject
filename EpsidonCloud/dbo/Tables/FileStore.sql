﻿CREATE TABLE [dbo].[FileStore] (
    [FileStoreId] INT IDENTITY (1, 1) NOT NULL,
    [FileId]      INT NULL,
    [StoreId]     INT NULL,
    CONSTRAINT [PK_FileStore] PRIMARY KEY CLUSTERED ([FileStoreId] ASC),
    CONSTRAINT [FK_FileStore_File] FOREIGN KEY ([FileId]) REFERENCES [dbo].[File] ([FileId]),
    CONSTRAINT [FK_FileStore_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

