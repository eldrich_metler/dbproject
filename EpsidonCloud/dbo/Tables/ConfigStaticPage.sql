﻿CREATE TABLE [dbo].[ConfigStaticPage] (
    [ConfigStaticPageId] INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]            INT             NULL,
    [PageTitle]          NVARCHAR (MAX)  NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    [FriendlyURL]        NVARCHAR (50)   NULL,
    [PageBody]           NVARCHAR (MAX)  NULL,
    [IsBodyHtml]         BIT             NULL,
    [ShowInMenu]         BIT             NULL,
    [ShowInHeader]       BIT             NULL,
    [ShowInFooter]       BIT             NULL,
    [MenuTitle]          NVARCHAR (30)   NULL,
    [Order]              INT             NULL,
    [Status]             INT             NULL,
    [LastUpdated]        DATETIME        NULL,
    [DateCreated]        DATETIME        NULL,
    [ExternalURL]        NVARCHAR (1000) NULL
);

