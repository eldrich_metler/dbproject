﻿CREATE TABLE [dbo].[RecurringBilling] (
    [RecurringBillingId] INT      IDENTITY (1, 1) NOT NULL,
    [StartDate]          DATETIME NULL,
    [FinishedDate]       DATETIME NULL,
    [RecordCount]        INT      NULL,
    [ErrorCount]         INT      NULL,
    [ExecutionTime]      INT      NULL,
    [IsMock]             BIT      NULL,
    [Status]             INT      NULL,
    [DateCreated]        DATETIME NULL,
    [LastUpdated]        DATETIME NULL,
    CONSTRAINT [PK_RecurringBilling_1] PRIMARY KEY CLUSTERED ([RecurringBillingId] ASC)
);

