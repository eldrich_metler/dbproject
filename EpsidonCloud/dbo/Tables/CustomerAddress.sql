﻿CREATE TABLE [dbo].[CustomerAddress] (
    [CustomerAddressId] INT IDENTITY (1, 1) NOT NULL,
    [CustomerId]        INT NULL,
    [AddressId]         INT NULL,
    CONSTRAINT [PK_CustomerAddress] PRIMARY KEY CLUSTERED ([CustomerAddressId] ASC),
    CONSTRAINT [FK_CustomerAddress_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_CustomerAddress_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);

