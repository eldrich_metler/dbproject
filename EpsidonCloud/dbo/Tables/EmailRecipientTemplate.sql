﻿CREATE TABLE [dbo].[EmailRecipientTemplate] (
    [EmailRecipientTemplateId] INT IDENTITY (1, 1) NOT NULL,
    [Blocked]                  BIT NOT NULL,
    [EmailTemplateId]          INT NOT NULL,
    [EmailRecipientId]         INT NULL,
    PRIMARY KEY CLUSTERED ([EmailRecipientTemplateId] ASC),
    FOREIGN KEY ([EmailRecipientId]) REFERENCES [dbo].[EmailRecipient] ([EmailRecipientId]),
    FOREIGN KEY ([EmailTemplateId]) REFERENCES [dbo].[EmailTemplate] ([EmailTemplateId])
);

