﻿CREATE TABLE [dbo].[EmailSetting] (
    [EmailSettingId]  INT           IDENTITY (1, 1) NOT NULL,
    [CompanyId]       INT           NULL,
    [StoreId]         INT           NULL,
    [Type]            INT           NULL,
    [Username]        NVARCHAR (50) NULL,
    [Password]        NVARCHAR (50) NULL,
    [Host]            NVARCHAR (50) NULL,
    [Port]            INT           NULL,
    [SendTestEmailTo] NVARCHAR (50) NULL,
    [LastUpdated]     DATETIME      NULL,
    [DateCreated]     DATETIME      NULL,
    [Status]          INT           NULL,
    CONSTRAINT [PK_EmailSetting] PRIMARY KEY CLUSTERED ([EmailSettingId] ASC),
    CONSTRAINT [FK_EmailSetting_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_EmailSetting_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

