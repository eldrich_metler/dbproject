﻿CREATE TABLE [dbo].[MarketplaceCategoryService] (
    [MarketplaceCategoryServiceId] INT      IDENTITY (1, 1) NOT NULL,
    [MarketplaceCategoryId]        INT      NULL,
    [ServiceId]                    INT      NULL,
    [StoreId]                      INT      NULL,
    [CompanyId]                    INT      NULL,
    [LastUpdated]                  DATETIME NULL,
    [DateCreated]                  DATETIME NULL,
    CONSTRAINT [PK_MarketplaceCategoryService] PRIMARY KEY CLUSTERED ([MarketplaceCategoryServiceId] ASC),
    CONSTRAINT [FK_MarketplaceCategoryService_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_MarketplaceCategoryService_MarketplaceCategory1] FOREIGN KEY ([MarketplaceCategoryId]) REFERENCES [dbo].[MarketplaceCategory] ([MarketplaceCategoryId]),
    CONSTRAINT [FK_MarketplaceCategoryService_Service1] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId]),
    CONSTRAINT [FK_MarketplaceCategoryService_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

