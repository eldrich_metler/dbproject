﻿CREATE TABLE [dbo].[ProviderConfigCurrency] (
    [ProviderConfigCurrencyId] INT IDENTITY (1, 1) NOT NULL,
    [ProviderId]               INT NULL,
    [ConfigCurrencyId]         INT NULL,
    CONSTRAINT [PK_ProviderConfigCurrency] PRIMARY KEY CLUSTERED ([ProviderConfigCurrencyId] ASC),
    CONSTRAINT [FK_ProviderConfigCurrency_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId]),
    CONSTRAINT [FK_ProviderConfigCurrency_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId])
);

