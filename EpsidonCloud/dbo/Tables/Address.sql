﻿CREATE TABLE [dbo].[Address] (
    [AddressId]       INT            IDENTITY (1, 1) NOT NULL,
    [AddressType]     INT            NULL,
    [Address1]        NVARCHAR (100) NULL,
    [Address2]        NVARCHAR (100) NULL,
    [ConfigCountryId] INT            NULL,
    [City]            NVARCHAR (50)  NULL,
    [PostalCode]      NVARCHAR (50)  NULL,
    [TelephoneNumber] NVARCHAR (50)  NULL,
    [FaxNumber]       NVARCHAR (50)  NULL,
    [LastUpdated]     DATETIME       NULL,
    [DateCreated]     DATETIME       NULL,
    [CellPhoneNumber] NVARCHAR (50)  NULL,
    [CompanyName]     NVARCHAR (500) NULL,
    [VatNumber]       NVARCHAR (50)  NULL,
    CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([AddressId] ASC),
    CONSTRAINT [FK_Address_ConfigCountry] FOREIGN KEY ([ConfigCountryId]) REFERENCES [dbo].[ConfigCountry] ([ConfigCountryId])
);





