﻿CREATE TABLE [dbo].[StorePaymentGateway] (
    [StorePaymentGatewayId] INT IDENTITY (1, 1) NOT NULL,
    [StoreId]               INT NULL,
    [PaymentGatewayId]      INT NULL,
    CONSTRAINT [PK_StorePaymentGateway] PRIMARY KEY CLUSTERED ([StorePaymentGatewayId] ASC),
    CONSTRAINT [FK_StorePaymentGateway_PaymentGateway] FOREIGN KEY ([PaymentGatewayId]) REFERENCES [dbo].[PaymentGateway] ([PaymentGatewayId]),
    CONSTRAINT [FK_StorePaymentGateway_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

