﻿CREATE TABLE [dbo].[WorkerTask] (
    [WorkerTaskId]      INT            IDENTITY (1, 1) NOT NULL,
    [TypeCode]          INT            NOT NULL,
    [TypeDescription]   VARCHAR (256)  NOT NULL,
    [Status]            INT            NOT NULL,
    [StatusName]        VARCHAR (16)   NOT NULL,
    [TaskMessage]       VARCHAR (4096) NOT NULL,
    [Reference]         VARCHAR (256)  NOT NULL,
    [RetryCounter]      INT            NOT NULL,
    [DateCreated]       DATETIME       NOT NULL,
    [DateLastProcessed] DATETIME       NULL,
    [DateCompleted]     DATETIME       NULL,
    CONSTRAINT [PK_WorkerTask] PRIMARY KEY CLUSTERED ([WorkerTaskId] ASC)
);

