﻿CREATE TABLE [dbo].[FileProvider] (
    [FileProviderId] INT IDENTITY (1, 1) NOT NULL,
    [FileId]         INT NULL,
    [ProviderId]     INT NULL,
    CONSTRAINT [PK_FileProvider] PRIMARY KEY CLUSTERED ([FileProviderId] ASC),
    CONSTRAINT [FK_FileProvider_File] FOREIGN KEY ([FileId]) REFERENCES [dbo].[File] ([FileId]),
    CONSTRAINT [FK_FileProvider_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId])
);

