﻿CREATE TABLE [dbo].[ConfigCurrency] (
    [ConfigCurrencyId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (100) NULL,
    [Symbol]           NVARCHAR (50)  NULL,
    [Status]           INT            NULL,
    [LastUpdated]      DATETIME       NULL,
    [DateCreated]      DATETIME       NULL,
    [Abbreviation]     CHAR (3)       NULL,
    CONSTRAINT [PK_CurrencyConfig] PRIMARY KEY CLUSTERED ([ConfigCurrencyId] ASC)
);

