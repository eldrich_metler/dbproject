﻿CREATE TABLE [dbo].[ConfigCountry] (
    [ConfigCountryId]      INT            IDENTITY (1, 1) NOT NULL,
    [Name]                 NVARCHAR (100) NULL,
    [Abbreviation]         NVARCHAR (50)  NULL,
    [Status]               INT            NULL,
    [UTCTimeOffsetMinutes] INT            NULL,
    [UTCTimeOffsetType]    INT            NULL,
    [LastUpdated]          DATETIME       NULL,
    [DateCreated]          DATETIME       NULL,
    CONSTRAINT [PK_CountryConfig] PRIMARY KEY CLUSTERED ([ConfigCountryId] ASC)
);

