﻿CREATE TABLE [dbo].[ConfigPayU] (
    [ConfigPayUId]     INT           NULL,
    [Username]         NVARCHAR (50) NULL,
    [Password]         NVARCHAR (50) NULL,
    [SafeKey]          NVARCHAR (50) NULL,
    [DateCreated]      DATETIME      NULL,
    [LastUpdated]      DATETIME      NULL,
    [PaymentGatewayId] INT           NULL,
    [Status]           INT           NULL
);



