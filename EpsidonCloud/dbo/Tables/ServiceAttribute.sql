﻿CREATE TABLE [dbo].[ServiceAttribute] (
    [ServiceAttributeId] INT            IDENTITY (1, 1) NOT NULL,
    [CartItemId]         INT            NULL,
    [OrderItemID]        INT            NULL,
    [AttributeJson]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ServiceAttribute] PRIMARY KEY CLUSTERED ([ServiceAttributeId] ASC)
);

