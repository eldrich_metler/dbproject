﻿CREATE TABLE [dbo].[MarginPolicy] (
    [MarginPolicyId]   INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]        INT             NULL,
    [ConfigCurrencyId] INT             NULL,
    [Margin]           DECIMAL(18, 3) NULL,
    [Status]           INT             NULL,
    [LastUpdated]      DATETIME        NULL,
    [DateCreated]      DATETIME        NULL,
    CONSTRAINT [PK_PricingPolicy] PRIMARY KEY CLUSTERED ([MarginPolicyId] ASC),
    CONSTRAINT [FK_MarginPolicy_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);



