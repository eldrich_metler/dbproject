﻿CREATE TABLE [dbo].[AgreementType] (
    [AgreementTypeId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (100) NULL,
    [Status]          INT            NULL,
    [LastUpdated]     DATETIME       NULL,
    [DateCreated]     DATETIME       NULL,
    CONSTRAINT [PK_AgreementType] PRIMARY KEY CLUSTERED ([AgreementTypeId] ASC)
);

