﻿CREATE TABLE [dbo].[Coupon] (
    [CouponId]           INT             IDENTITY (1, 1) NOT NULL,
    [ConfigCurrencyId]   INT             NULL,
    [StoreId]            INT             NULL,
    [CouponCode]         NVARCHAR (10)   NULL,
    [CouponType]         INT             NULL,
    [Description]        NVARCHAR (MAX)  NULL,
    [StartDate]          DATETIME        NULL,
    [EndDate]            DATETIME        NULL,
    [Discount]           DECIMAL (18, 2) NULL,
    [PercentageDiscount] INT             NULL,
    [Status]             INT             NULL,
    [LastUpdated]        DATETIME        NULL,
    [DateCreated]        DATETIME        NULL,
    CONSTRAINT [PK_Coupon] PRIMARY KEY CLUSTERED ([CouponId] ASC),
    CONSTRAINT [FK_Coupon_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

