﻿CREATE TABLE [dbo].[AzureConfig] (
    [AzureConfigId]       INT            IDENTITY (1, 1) NOT NULL,
    [StoreId]             INT            NOT NULL,
    [AzureResellerMargin] DECIMAL (8, 3) NULL,
    [AzureCustomerMargin] DECIMAL (8, 3) NULL,
    [DateCreated]         DATE           NOT NULL,
    [LastUpdated]         DATE           NOT NULL,
    PRIMARY KEY CLUSTERED ([AzureConfigId] ASC),
    CONSTRAINT [FK_AzureConfig_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

