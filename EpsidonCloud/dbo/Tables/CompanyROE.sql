﻿CREATE TABLE [dbo].[CompanyROE] (
    [CompanyROEId]   INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]      INT             NULL,
    [CurrencyFromId] INT             NULL,
    [CurrencyToId]   INT             NULL,
    [Rate]           DECIMAL (18, 6) NULL,
    [DateCreated]    DATETIME        NULL,
    [LastUpdated]    DATETIME        NOT NULL,
    CONSTRAINT [PK_ProviderROE] PRIMARY KEY CLUSTERED ([CompanyROEId] ASC),
    CONSTRAINT [FK_CompanyROE_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);

