﻿CREATE TABLE [dbo].[FileService] (
    [FileServiceId] INT IDENTITY (1, 1) NOT NULL,
    [FileId]        INT NULL,
    [ServiceId]     INT NULL,
    CONSTRAINT [PK_FileService] PRIMARY KEY CLUSTERED ([FileServiceId] ASC),
    CONSTRAINT [FK_FileService_File] FOREIGN KEY ([FileId]) REFERENCES [dbo].[File] ([FileId]),
    CONSTRAINT [FK_FileService_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

