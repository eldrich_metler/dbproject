﻿CREATE TABLE [dbo].[MicrosoftSubscription] (
    [MicrosoftSubscriptionId] INT             IDENTITY (1, 1) NOT NULL,
    [MicrosoftCustomerId]     INT             NULL,
    [SubscriptionNumber]      NVARCHAR (30)   NULL,
    [CustomerId]              INT             NULL,
    [OrderItemId]             INT             NULL,
    [Quantity]                INT             NULL,
    [Amount]                  DECIMAL (18, 2) NULL,
    [BillingCycle]            NVARCHAR (50)   NULL,
    [NextBillingRun]          DATETIME        NULL,
    [PreviousBillingRun]      DATETIME        NULL,
    [BilledDate]              DATETIME        NULL,
    [Retries]                 INT             NULL,
    [FriendlyName]            NVARCHAR (100)  NULL,
    [Status]                  INT             NULL,
    [MicrosoftStatus]         INT             NULL,
    [SubscriptionId]          NVARCHAR (50)   NULL,
    [OfferId]                 NVARCHAR (50)   NULL,
    [DateCreated]             DATETIME        NULL,
    [PaymentType]             INT             NULL,
    [PaymentGateway]          INT             NULL,
    [OrderNumber]             NVARCHAR (20)   NULL,
    [PriceLockDuration]       INT             NULL,
    [PriceLockStartDate]      DATETIME        NULL,
    [SubscriptionFrom]        DATETIME        NULL,
    [SubscriptionTo]          DATETIME        NULL,
    [PaymentDescription]      NVARCHAR (1000) NULL,
    [VendorPrice]             DECIMAL (18, 6) NULL,
    [CompanyOfferPrice]       DECIMAL (18, 6) NULL,
    [CompanyOfferMargin]      DECIMAL (18, 6) NULL,
    [ResellerPrice]           DECIMAL (18, 6) NULL,
    [StoreOfferPrice]         DECIMAL (18, 6) NULL,
    [StoreOfferMargin]        DECIMAL (18, 6) NULL,
    [CompanyROE]              DECIMAL (18, 6) NULL,
    [StoreROE]                DECIMAL (18, 6) NULL,
    [VAT]                     DECIMAL (18, 6) NULL,
    [ProductPrice]            DECIMAL (18, 2) NULL,
    [SubscriptionActivity]    INT             NULL,
    [ActivityQuantity]        INT             NULL,
    [MicrosoftOrderId]        NVARCHAR (100)  NULL,
    CONSTRAINT [PK_MicrosoftSubscription] PRIMARY KEY CLUSTERED ([MicrosoftSubscriptionId] ASC),
    CONSTRAINT [FK_MicrosoftSubscription_MicrosoftSubscription] FOREIGN KEY ([MicrosoftCustomerId]) REFERENCES [dbo].[MicrosoftCustomer] ([MicrosoftCustomerId])
);







