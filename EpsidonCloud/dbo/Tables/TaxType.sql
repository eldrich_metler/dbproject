﻿CREATE TABLE [dbo].[TaxType] (
    [TaxTypeId]   INT           IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR (50) NULL,
    [Status]      INT           NULL,
    [LastUpdated] DATETIME      NULL,
    [DateCreated] DATETIME      NULL,
    CONSTRAINT [PK_TaxType] PRIMARY KEY CLUSTERED ([TaxTypeId] ASC)
);

