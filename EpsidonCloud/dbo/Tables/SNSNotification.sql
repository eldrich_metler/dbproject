﻿CREATE TABLE [dbo].[SNSNotification] (
    [SNSNotificationId] INT            NOT NULL,
    [Type]              INT            NOT NULL,
    [MessageId]         NVARCHAR (50)  NULL,
    [Token]             NVARCHAR (MAX) NULL,
    [TopicArn]          NVARCHAR (MAX) NULL,
    [Subject]           NVARCHAR (MAX) NULL,
    [SubscribeURL]      NVARCHAR (MAX) NULL,
    [SignatureVersion]  NVARCHAR (MAX) NULL,
    [Signature]         NVARCHAR (MAX) NULL,
    [SigningCertURL]    NVARCHAR (MAX) NULL,
    [UnsubscribeURL]    NVARCHAR (MAX) NULL,
    [Bounce]            NVARCHAR (MAX) NULL,
    [Timestamp]         DATE           NOT NULL,
    PRIMARY KEY CLUSTERED ([SNSNotificationId] ASC)
);

