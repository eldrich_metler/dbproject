﻿CREATE TABLE [dbo].[CustomerMarginPolicy] (
    [MarginPolicyId] INT NOT NULL,
    [StoreId]        INT NOT NULL,
    [CustomerId]     INT NOT NULL,
    [ProviderId]     INT NULL,
    CONSTRAINT [PK_CustomerPricingPolicy] PRIMARY KEY CLUSTERED ([MarginPolicyId] ASC),
    CONSTRAINT [FK_CustomerMarginPolicy_MarginPolicy] FOREIGN KEY ([MarginPolicyId]) REFERENCES [dbo].[MarginPolicy] ([MarginPolicyId]),
    CONSTRAINT [FK_CustomerMarginPolicy_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId]),
    CONSTRAINT [FK_CustomerPricingPolicy_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_CustomerPricingPolicy_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

