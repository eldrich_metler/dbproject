﻿CREATE TABLE [dbo].[LanguageCode] (
    [LanguageCodeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (50) NULL,
    [Code]           NVARCHAR (50) NULL,
    [TextDirection]  INT           NULL,
    [Status]         INT           NULL,
    [DateCreated]    DATETIME      NULL,
    [LastUpdated]    DATETIME      NULL,
    CONSTRAINT [PK_LanguageCode] PRIMARY KEY CLUSTERED ([LanguageCodeId] ASC)
);

