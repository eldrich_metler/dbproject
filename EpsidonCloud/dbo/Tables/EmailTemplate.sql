﻿CREATE TABLE [dbo].[EmailTemplate] (
    [EmailTemplateId]       INT            IDENTITY (1, 1) NOT NULL,
    [ParentEmailTemplateId] INT            NULL,
    [CompanyId]             INT            NULL,
    [StoreId]               INT            NULL,
    [Type]                  INT            NULL,
    [TemplateName]          INT            NULL,
    [Name]                  NVARCHAR (50)  NULL,
    [Subject]               NVARCHAR (MAX) NULL,
    [Body]                  NVARCHAR (MAX) NULL,
    [IsBodyHtml]            BIT            NULL,
    [SenderDisplayName]     NVARCHAR (50)  NULL,
    [EmailFrom]             NVARCHAR (MAX) NULL,
    [EmailCC]               NVARCHAR (MAX) NULL,
    [EmailBCC]              NVARCHAR (MAX) NULL,
    [IsMaster]              BIT            NULL,
    [Status]                INT            NULL,
    [LastUpdated]           DATETIME       NULL,
    [DateCreated]           DATETIME       NULL,
    [UseEmailBCC]           BIT            NULL,
    [BlockSendToCustomer]   BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED ([EmailTemplateId] ASC),
    CONSTRAINT [FK_EmailTemplate_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_EmailTemplate_EmailTemplate] FOREIGN KEY ([ParentEmailTemplateId]) REFERENCES [dbo].[EmailTemplate] ([EmailTemplateId]),
    CONSTRAINT [FK_EmailTemplate_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);



