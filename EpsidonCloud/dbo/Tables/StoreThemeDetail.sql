﻿CREATE TABLE [dbo].[StoreThemeDetail] (
    [StoreThemeDetailId] INT            IDENTITY (1, 1) NOT NULL,
    [StoreThemeId]       INT            NULL,
    [ThemeDetailId]      INT            NULL,
    [Value]              VARCHAR (5000) NULL,
    [LastUpdated]        DATETIME       NULL,
    [DateCreated]        DATETIME       NULL,
    CONSTRAINT [PK_StoreThemeDetail_1] PRIMARY KEY CLUSTERED ([StoreThemeDetailId] ASC),
    CONSTRAINT [FK_StoreThemeDetail_StoreTheme] FOREIGN KEY ([StoreThemeId]) REFERENCES [dbo].[StoreTheme] ([StoreThemeId]),
    CONSTRAINT [FK_StoreThemeDetail_ThemeDetail] FOREIGN KEY ([ThemeDetailId]) REFERENCES [dbo].[ThemeDetail] ([ThemeDetailId])
);

