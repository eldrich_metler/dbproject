﻿CREATE TABLE [dbo].[FileCompany] (
    [FileCompanyId] INT IDENTITY (1, 1) NOT NULL,
    [FileId]        INT NULL,
    [CompanyId]     INT NULL,
    CONSTRAINT [PK_FileCompany] PRIMARY KEY CLUSTERED ([FileCompanyId] ASC),
    CONSTRAINT [FK_FileCompany_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_FileCompany_File] FOREIGN KEY ([FileId]) REFERENCES [dbo].[File] ([FileId])
);

