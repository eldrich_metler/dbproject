﻿CREATE TABLE [dbo].[EmailQueue] (
    [EmailQueueId]        INT            IDENTITY (1, 1) NOT NULL,
    [EmailTemplateId]     INT            NULL,
    [EmailSettingId]      INT            NULL,
    [CompanyId]           INT            NULL,
    [StoreId]             INT            NULL,
    [Subject]             NVARCHAR (200) NULL,
    [Body]                NVARCHAR (MAX) NULL,
    [IsBodyHtml]          BIT            NULL,
    [SenderDisplayName]   NVARCHAR (50)  NULL,
    [EmailTo]             NVARCHAR (MAX) NULL,
    [EmailCC]             NVARCHAR (MAX) NULL,
    [EmailBCC]            NVARCHAR (MAX) NULL,
    [IsSent]              BIT            NULL,
    [SendAttempts]        INT            NULL,
    [LastSendAttemptDate] DATETIME       NULL,
    [Status]              INT            NULL,
    [LastUpdated]         DATETIME       NULL,
    [DateCreated]         DATETIME       NULL,
    [UserAccountId]       INT            NULL,
    CONSTRAINT [PK_EmailQueue] PRIMARY KEY CLUSTERED ([EmailQueueId] ASC),
    CONSTRAINT [FK_EmailQueue_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_EmailQueue_EmailSetting] FOREIGN KEY ([EmailSettingId]) REFERENCES [dbo].[EmailSetting] ([EmailSettingId]),
    CONSTRAINT [FK_EmailQueue_EmailTemplate] FOREIGN KEY ([EmailTemplateId]) REFERENCES [dbo].[EmailTemplate] ([EmailTemplateId]),
    CONSTRAINT [FK_EmailQueue_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId]),
    CONSTRAINT [FK_EmailQueue_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);



