﻿CREATE TABLE [dbo].[Store] (
    [StoreId]                  INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]                INT             NOT NULL,
    [Domain]                   NVARCHAR (50)   NOT NULL,
    [Name]                     NVARCHAR (500)  NULL,
    [URL]                      NVARCHAR (4000) NULL,
    [MarketplaceCategoryJson]  NVARCHAR (MAX)  NULL,
    [UseOwnCategoryStructure]  BIT             NOT NULL,
    [Logo]                     NVARCHAR (50)   NULL,
    [APIKey]                   NVARCHAR (MAX)  NULL,
    [IsIISCreated]             INT             NOT NULL,
    [Twitter]                  NVARCHAR (1000) NULL,
    [Facebook]                 NVARCHAR (1000) NULL,
    [LinkedIn]                 NVARCHAR (1000) NULL,
    [GooglePlus]               NVARCHAR (1000) NULL,
    [SignUpText]               NVARCHAR (2000) NULL,
    [EmailAddress]             NVARCHAR (50)   NULL,
    [Status]                   INT             NOT NULL,
    [LastUpdated]              DATETIME        NULL,
    [DateCreated]              DATETIME        NULL,
    [VAT]                      DECIMAL (18, 2) NULL,
    [ConfigCurrencyId]         INT             NULL,
    [TaxType]                  INT             NULL,
    [StylesheetURL]            NVARCHAR (500)  NOT NULL,
    [FooterScript]             NVARCHAR (MAX)  NULL,
    [UseOwnBilling]            BIT             NOT NULL,
    [Instagram]                NVARCHAR (1000) NULL,
    [Pinterest]                NVARCHAR (1000) NULL,
    [Country]                  NVARCHAR (100)  NULL,
    [SetupType]                INT             NOT NULL,
    [ContactUs]                NVARCHAR (50)   NULL,
    [AccountNumber]            NVARCHAR (20)   NULL,
    [SetupTypeOtherPercentage] DECIMAL (8, 2)  NULL,
    [WhatsApp]                 NVARCHAR (100)  NULL,
    [CustomerResetEmail]       BIT             NOT NULL,
    [SwitchOffSignUp]          BIT             NOT NULL,
    [DisplayStorePricing]      BIT             CONSTRAINT [DF_Store_DisplayStorePricing] DEFAULT ((1)) NOT NULL,
    [AllowAzureCustomerCreate] BIT             NOT NULL,
    [AzureStoreMargin]         DECIMAL (8, 3)  NULL,
    [SwitchOffLogin]           BIT             NOT NULL,
    [ThemeType]                INT             NOT NULL,
    CONSTRAINT [PK_Store] PRIMARY KEY CLUSTERED ([StoreId] ASC)
);













