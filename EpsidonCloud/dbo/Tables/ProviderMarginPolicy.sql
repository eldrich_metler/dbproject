﻿CREATE TABLE [dbo].[ProviderMarginPolicy] (
    [MarginPolicyId] INT NOT NULL,
    [StoreId]        INT NOT NULL,
    [ProviderId]     INT NOT NULL,
    CONSTRAINT [PK_ProviderPricingPolicy] PRIMARY KEY CLUSTERED ([MarginPolicyId] ASC),
    CONSTRAINT [FK_ProviderMarginPolicy_MarginPolicy] FOREIGN KEY ([MarginPolicyId]) REFERENCES [dbo].[MarginPolicy] ([MarginPolicyId]),
    CONSTRAINT [FK_ProviderMarginPolicy_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId]),
    CONSTRAINT [FK_ProviderPricingPolicy_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId])
);

