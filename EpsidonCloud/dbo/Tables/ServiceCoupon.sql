﻿CREATE TABLE [dbo].[ServiceCoupon] (
    [ServiceCouponId] INT IDENTITY (1, 1) NOT NULL,
    [ServiceId]       INT NULL,
    [CouponId]        INT NULL,
    CONSTRAINT [PK_ServiceCoupon] PRIMARY KEY CLUSTERED ([ServiceCouponId] ASC),
    CONSTRAINT [FK_ServiceCoupon_Coupon] FOREIGN KEY ([CouponId]) REFERENCES [dbo].[Coupon] ([CouponId]),
    CONSTRAINT [FK_ServiceCoupon_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

