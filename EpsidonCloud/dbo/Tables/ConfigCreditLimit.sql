﻿CREATE TABLE [dbo].[ConfigCreditLimit] (
    [ConfigCreditLimitId]     INT             NULL,
    [PaymentGatewayId]        INT             NULL,
    [CreditLimit]             DECIMAL (18, 2) NULL,
    [CreditLimitType]         INT             NULL,
    [LimitExtension]          DECIMAL (18, 2) NULL,
    [ExtensionExpirationDate] DATETIME        NULL,
    [Status]                  INT             NULL,
    [DateCreated]             DATETIME        NULL,
    [LastUpdated]             DATETIME        NULL
);



