﻿CREATE TABLE [dbo].[CompanyService] (
    [CompanyServiceId] INT IDENTITY (1, 1) NOT NULL,
    [CompanyId]        INT NULL,
    [ServiceId]        INT NULL,
    [SetOwnPrice]      BIT NULL,
    CONSTRAINT [PK_CompanyService] PRIMARY KEY CLUSTERED ([CompanyServiceId] ASC),
    CONSTRAINT [FK_CompanyService_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_CompanyService_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

