﻿CREATE TABLE [dbo].[PaymentGateway] (
    [PaymentGatewayId] INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (50)  NULL,
    [Type]             INT            NULL,
    [CurrencyList]     NVARCHAR (MAX) NULL,
    [DateCreated]      DATETIME       NULL,
    [LastUpdated]      DATETIME       NULL,
    [Status]           INT            NULL,
    CONSTRAINT [PK_PaymentGateway] PRIMARY KEY CLUSTERED ([PaymentGatewayId] ASC)
);

