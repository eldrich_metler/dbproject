﻿CREATE TABLE [dbo].[AuditReportLoginAs] (
    [AuditReportLoginAsID]      INT            IDENTITY (1, 1) NOT NULL,
    [Site]                      NVARCHAR (MAX) NULL,
    [LoggedInAs]                NVARCHAR (500) NULL,
    [LoggedInAsUserAccountRole] NVARCHAR (50)  NULL,
    [LoggedInAsUserAccountId]   INT            NULL,
    [UserAccountId]             INT            NULL,
    [Username]                  NVARCHAR (500) NULL,
    [UserAccountRole]           NVARCHAR (50)  NULL,
    [FromPortal]                NVARCHAR (MAX) NULL,
    [DateCreated]               DATETIME       NULL,
    CONSTRAINT [PK_AuditReportLoginAs] PRIMARY KEY CLUSTERED ([AuditReportLoginAsID] ASC)
);

