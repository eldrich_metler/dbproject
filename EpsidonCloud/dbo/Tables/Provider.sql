﻿CREATE TABLE [dbo].[Provider] (
    [ProviderId]                   INT             IDENTITY (1, 1) NOT NULL,
    [CompanyId]                    INT             NULL,
    [Name]                         NVARCHAR (100)  NULL,
    [Description]                  NVARCHAR (500)  NULL,
    [Status]                       INT             NULL,
    [Logo]                         NVARCHAR (50)   NULL,
    [ProvisioningEmailAddressList] NVARCHAR (3000) NULL,
    [SEOTitle]                     NVARCHAR (1000) NULL,
    [SEODescription]               NVARCHAR (4000) NULL,
    [SEOKeywords]                  NVARCHAR (4000) NULL,
    [LastUpdated]                  DATETIME        NULL,
    [DateCreated]                  DATETIME        NULL,
    [StoreId]                      INT             NULL,
    [ManualProvisioningSteps]      NVARCHAR (MAX)  NULL,
    [ProviderThemeColor]           NVARCHAR (50)   NULL,
    CONSTRAINT [PK_Provider] PRIMARY KEY CLUSTERED ([ProviderId] ASC),
    CONSTRAINT [FK_Provider_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);



