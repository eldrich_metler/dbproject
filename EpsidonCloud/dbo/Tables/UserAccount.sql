﻿CREATE TABLE [dbo].[UserAccount] (
    [UserAccountId]       INT            IDENTITY (1, 1) NOT NULL,
    [UserAccountRole]     INT            NULL,
    [UserAccountStatus]   INT            NULL,
    [Username]            NVARCHAR (50)  NULL,
    [EmailAddress]        NVARCHAR (50)  NULL,
    [PasswordHash]        NVARCHAR (500) NULL,
    [PasswordSalt]        NVARCHAR (500) NULL,
    [FirstName]           NVARCHAR (100) NULL,
    [LastName]            NVARCHAR (100) NULL,
    [CreatedById]         INT            NULL,
    [LastLoggedIn]        DATETIME       NULL,
    [DefaultDateFormat]   NVARCHAR (50)  NULL,
    [DateCreated]         DATETIME       NULL,
    [LastUpdated]         DATETIME       NULL,
    [AllowPricingUpdates] BIT            NULL,
    [AllowProxyAs]        BIT            NULL,
    CONSTRAINT [PK_UserAccount] PRIMARY KEY CLUSTERED ([UserAccountId] ASC)
);

