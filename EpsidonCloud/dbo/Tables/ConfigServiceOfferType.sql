﻿CREATE TABLE [dbo].[ConfigServiceOfferType] (
    [ConfigServiceOfferTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]                     NVARCHAR (50) NULL,
    [Status]                   INT           NULL,
    [Type]                     INT           NULL,
    [LastUpdated]              DATETIME      NULL,
    [DateCreated]              DATETIME      NULL,
    CONSTRAINT [PK_ConfigServiceOfferType] PRIMARY KEY CLUSTERED ([ConfigServiceOfferTypeId] ASC)
);

