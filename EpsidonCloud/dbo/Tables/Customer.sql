﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]        INT            IDENTITY (1, 1) NOT NULL,
    [UserAccountId]     INT            NULL,
    [LastUpdated]       DATETIME       NULL,
    [DateCreated]       DATETIME       NULL,
    [StoreUrl]          NVARCHAR (100) NULL,
    [ParentCustomerId]  INT            NULL,
    [AccountIdentifier] NVARCHAR (100) NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);



