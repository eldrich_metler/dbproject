﻿CREATE TABLE [dbo].[CartItem] (
    [CartItemId]      INT      IDENTITY (1, 1) NOT NULL,
    [CartId]          INT      NULL,
    [ServiceId]       INT      NULL,
    [ProductQuantity] INT      NULL,
    [DateCreated]     DATETIME NULL,
    [LastUpdated]     DATETIME NULL,
    [ServiceOfferId]  INT      NULL,
    CONSTRAINT [PK_CartItem] PRIMARY KEY CLUSTERED ([CartItemId] ASC),
    CONSTRAINT [FK_CartItem_Cart] FOREIGN KEY ([CartId]) REFERENCES [dbo].[Cart] ([CartId]),
    CONSTRAINT [FK_CartItem_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);

