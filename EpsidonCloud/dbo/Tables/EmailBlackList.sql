﻿CREATE TABLE [dbo].[EmailBlackList] (
    [EmailBlackListId]     INT            IDENTITY (1, 1) NOT NULL,
    [EmailAddress]         NVARCHAR (50)  NOT NULL,
    [Action]               NVARCHAR (50)  NULL,
    [Status]               NVARCHAR (50)  NULL,
    [ResolvedComment]      NVARCHAR (200) NULL,
    [DiagnosticCode]       NVARCHAR (MAX) NULL,
    [EmailBlackListStatus] INT            NOT NULL,
    [DateCreated]          DATE           NOT NULL,
    [DateUpdated]          DATE           NOT NULL,
    [ResolvedById]         INT            NULL,
    PRIMARY KEY CLUSTERED ([EmailBlackListId] ASC),
    FOREIGN KEY ([ResolvedById]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

