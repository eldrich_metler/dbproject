﻿CREATE TABLE [dbo].[ThemeDetail] (
    [ThemeDetailId] INT            IDENTITY (1, 1) NOT NULL,
    [ThemeId]       INT            NULL,
    [Name]          VARCHAR (50)   NULL,
    [Variable]      VARCHAR (50)   NULL,
    [DefaultValue]  VARCHAR (5000) NULL,
    [LastUpdated]   DATETIME       NULL,
    [DateCreated]   DATETIME       NULL,
    CONSTRAINT [PK_StoreThemeDetail] PRIMARY KEY CLUSTERED ([ThemeDetailId] ASC),
    CONSTRAINT [FK_ThemeDetail_Theme] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Theme] ([ThemeId])
);

