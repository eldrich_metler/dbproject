﻿CREATE TABLE [dbo].[StoreService] (
    [StoreServiceId] INT IDENTITY (1, 1) NOT NULL,
    [StoreId]        INT NULL,
    [ServiceId]      INT NULL,
    CONSTRAINT [PK_StoreService] PRIMARY KEY CLUSTERED ([StoreServiceId] ASC),
    CONSTRAINT [FK_StoreService_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId]),
    CONSTRAINT [FK_StoreService_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

