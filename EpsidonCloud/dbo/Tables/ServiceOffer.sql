﻿CREATE TABLE [dbo].[ServiceOffer] (
    [ServiceOfferId]      INT            IDENTITY (1, 1) NOT NULL,
    [ServiceId]           INT            NULL,
    [Name]                NVARCHAR (150) NULL,
    [Description]         NVARCHAR (500) NULL,
    [MinimumQuantity]     INT            NULL,
    [UnitPriceQuantity]   INT            NULL,
    [IsProRate]           INT            NULL,
    [PricingBillingCycle] INT            NULL,
    [SKU]                 NVARCHAR (100) NULL,
    [Status]              INT            NULL,
    [LastUpdated]         DATETIME       NULL,
    [DateCreated]         DATETIME       NULL,
    [Type]                INT            NULL,
    [Usage]               INT            NULL,
    [Order]               INT            NULL,
    [Source]              INT            NULL,
    [AgreementTypeId]     INT            NULL,
    CONSTRAINT [PK_ServiceOffer] PRIMARY KEY CLUSTERED ([ServiceOfferId] ASC),
    CONSTRAINT [FK_ServiceOffer_AgreementType] FOREIGN KEY ([AgreementTypeId]) REFERENCES [dbo].[AgreementType] ([AgreementTypeId]),
    CONSTRAINT [FK_ServiceOffer_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId])
);






GO


