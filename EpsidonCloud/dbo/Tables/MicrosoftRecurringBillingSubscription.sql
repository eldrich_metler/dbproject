﻿CREATE TABLE [dbo].[MicrosoftRecurringBillingSubscription] (
    [MicrosoftRecurringBillingSubscriptionId] INT IDENTITY (1, 1) NOT NULL,
    [RecurringBillingId]                      INT NULL,
    [MicrosoftSubscriptionId]                 INT NULL,
    CONSTRAINT [PK_MicrosoftRecurringBillingSubscription] PRIMARY KEY CLUSTERED ([MicrosoftRecurringBillingSubscriptionId] ASC)
);

