﻿CREATE TABLE [dbo].[Audit] (
    [AuditId]                INT            IDENTITY (1, 1) NOT NULL,
    [UserAccountId]          INT            NULL,
    [TableName]              NVARCHAR (500) NULL,
    [RecordId]               INT            NULL,
    [AuditType]              INT            NULL,
    [DateCreated]            DATETIME       NULL,
    [LastUpdated]            DATETIME       NULL,
    [UserAccountProxiedInId] INT            NULL,
    [AuditReportLoginAsId]   INT            NULL,
    CONSTRAINT [PK_Audit] PRIMARY KEY CLUSTERED ([AuditId] ASC),
    CONSTRAINT [FK_Audit_AuditReportLoginAs] FOREIGN KEY ([AuditReportLoginAsId]) REFERENCES [dbo].[AuditReportLoginAs] ([AuditReportLoginAsID]),
    CONSTRAINT [FK_Audit_ProxiedIdUserAccount] FOREIGN KEY ([UserAccountProxiedInId]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    CONSTRAINT [FK_Audit_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);





