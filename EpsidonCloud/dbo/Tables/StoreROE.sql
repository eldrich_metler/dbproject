﻿CREATE TABLE [dbo].[StoreROE] (
    [StoreROEId]     INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]        INT             NULL,
    [CurrencyFromId] INT             NULL,
    [CurrencyToId]   INT             NULL,
    [Rate]           DECIMAL (18, 4) NULL,
    [DateCreated]    DATETIME        NULL,
    [LastUpdated]    DATETIME        NULL,
    CONSTRAINT [PK_StoreROE] PRIMARY KEY CLUSTERED ([StoreROEId] ASC),
    CONSTRAINT [FK_StoreROE_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

