﻿CREATE TABLE [dbo].[StagingStoreService] (
    [StagingStoreServiceId]        INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]                      INT             NULL,
    [ServiceId]                    INT             NULL,
    [ProviderId]                   INT             NULL,
    [IsProviderMarginPrice]        BIT             CONSTRAINT [DF_StagingStoreService_IsProviderPrice] DEFAULT ((0)) NOT NULL,
    [CustomerId]                   INT             NULL,
    [CustomerProviderId]           INT             NULL,
    [CompanyVendorPrice]           DECIMAL (18, 2) NULL,
    [CompanyOfferPrice]            DECIMAL (18, 2) NULL,
    [CompanyOfferMargin]           DECIMAL (18, 2) NULL,
    [CompanyResellerPrice]         DECIMAL (18, 2) NULL,
    [SubCompanyVendorPrice]        DECIMAL (18, 2) NULL,
    [SubCompanyOfferPrice]         DECIMAL (18, 2) NULL,
    [SubCompanyOfferMargin]        DECIMAL (18, 2) NULL,
    [SubCompanyResellerPrice]      DECIMAL (18, 2) NULL,
    [StoreVendorPrice]             DECIMAL (18, 2) NULL,
    [StoreOfferPrice]              DECIMAL (18, 2) NULL,
    [StoreOfferMargin]             DECIMAL (18, 2) NULL,
    [StorePriceLockDuration]       INT             NULL,
    [StorePriceLockPrice]          DECIMAL (18, 2) NULL,
    [StoreVAT]                     DECIMAL (18, 2) NULL,
    [StoreTradeConfigCurrencyId]   INT             NULL,
    [StoreEndUserConfigCurrencyId] INT             NULL,
    [CompanyRoeRate]               DECIMAL (18, 4) NULL,
    [StoreRoeRate]                 DECIMAL (18, 4) NULL,
    [VendorConfigCurrencyId]       INT             NULL,
    [ServiceName]                  NVARCHAR (250)  NULL,
    [ServiceOfferId]               INT             NULL,
    [ServiceOfferPrice]            DECIMAL (18, 4) NULL,
    [ServiceOfferVatPrice]         DECIMAL (18, 4) NULL,
    [DateCreated]                  DATETIME        NOT NULL,
    CONSTRAINT [PK_StagingStoreService] PRIMARY KEY CLUSTERED ([StagingStoreServiceId] ASC)
);









