﻿CREATE TABLE [dbo].[StoreServicePricing] (
    [StoreServicePricingId] INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]               INT             NULL,
    [ServiceId]             INT             NULL,
    [CompanyMargin]         DECIMAL (18, 2) NULL,
    [StoreMargin]           DECIMAL (18, 2) NULL,
    [LastUpdated]           DATETIME        NULL,
    [DateCreated]           DATETIME        NULL,
    [Status]                INT             NULL,
    CONSTRAINT [PK_StoreServicePricing] PRIMARY KEY CLUSTERED ([StoreServicePricingId] ASC)
);

