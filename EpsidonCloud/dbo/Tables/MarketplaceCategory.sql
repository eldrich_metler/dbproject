﻿CREATE TABLE [dbo].[MarketplaceCategory] (
    [MarketplaceCategoryId]       INT            IDENTITY (1, 1) NOT NULL,
    [ParentMarketplaceCategoryId] INT            NULL,
    [StoreId]                     INT            NULL,
    [CompanyId]                   INT            NULL,
    [Name]                        NVARCHAR (100) NULL,
    [Order]                       INT            NULL,
    [Depth]                       INT            NULL,
    [Status]                      INT            NULL,
    [Type]                        INT            NULL,
    [DateCreated]                 DATETIME       NULL,
    [LastUpdated]                 DATETIME       NULL,
    CONSTRAINT [PK_MarketplaceCategory] PRIMARY KEY CLUSTERED ([MarketplaceCategoryId] ASC),
    CONSTRAINT [FK_MarketplaceCategory_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_MarketplaceCategory_MarketplaceCategory] FOREIGN KEY ([ParentMarketplaceCategoryId]) REFERENCES [dbo].[MarketplaceCategory] ([MarketplaceCategoryId]),
    CONSTRAINT [FK_MarketplaceCategory_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);


GO
ALTER TABLE [dbo].[MarketplaceCategory] NOCHECK CONSTRAINT [FK_MarketplaceCategory_Store];

