﻿CREATE TABLE [dbo].[StoreMarginPolicy] (
    [StoreMarginPolicyId] INT IDENTITY (1, 1) NOT NULL,
    [MarginPolicyId]      INT NOT NULL,
    [StoreId]             INT NOT NULL,
    CONSTRAINT [PK_StorePricingPolicy] PRIMARY KEY CLUSTERED ([StoreMarginPolicyId] ASC),
    CONSTRAINT [FK_StoreMarginPolicy_MarginPolicy] FOREIGN KEY ([MarginPolicyId]) REFERENCES [dbo].[MarginPolicy] ([MarginPolicyId]),
    CONSTRAINT [FK_StoreMarginPolicy_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);



