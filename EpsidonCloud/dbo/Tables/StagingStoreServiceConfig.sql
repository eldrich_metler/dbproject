﻿CREATE TABLE [dbo].[StagingStoreServiceConfig] (
    [StagingStoreServiceConfigId] INT IDENTITY (1, 1) NOT NULL,
    [IsRunning]                   BIT NULL,
    CONSTRAINT [PK_StagingStoreServiceConfig] PRIMARY KEY CLUSTERED ([StagingStoreServiceConfigId] ASC)
);



