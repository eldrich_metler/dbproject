﻿CREATE TABLE [dbo].[EmailRecipient] (
    [EmailRecipientId] INT      IDENTITY (1, 1) NOT NULL,
    [UserAccountId]    INT      NOT NULL,
    [Status]           INT      NOT NULL,
    [DateCreated]      DATETIME NOT NULL,
    [LastUpdated]      DATETIME NOT NULL,
    [CreatedById]      INT      NOT NULL,
    [LastUpdatedById]  INT      NOT NULL,
    PRIMARY KEY CLUSTERED ([EmailRecipientId] ASC),
    FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    FOREIGN KEY ([LastUpdatedById]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId]),
    UNIQUE NONCLUSTERED ([EmailRecipientId] ASC, [Status] ASC)
);

