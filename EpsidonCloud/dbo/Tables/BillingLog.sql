﻿CREATE TABLE [dbo].[BillingLog] (
    [BillingLogId]       INT            IDENTITY (1, 1) NOT NULL,
    [SubscriptionId]     INT            NULL,
    [RecurringBillingId] INT            NULL,
    [CompanyId]          INT            NULL,
    [StoreId]            INT            NULL,
    [ProviderId]         INT            NULL,
    [BillingDate]        DATETIME       NULL,
    [Process]            NVARCHAR (50)  NULL,
    [Type]               INT            NULL,
    [Level]              INT            NULL,
    [Message]            NVARCHAR (MAX) NULL,
    [DateCreated]        DATETIME       NULL,
    CONSTRAINT [PK_BillingLog] PRIMARY KEY CLUSTERED ([BillingLogId] ASC),
    CONSTRAINT [FK_BillingLog_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_BillingLog_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId]),
    CONSTRAINT [FK_BillingLog_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId]),
    CONSTRAINT [FK_BillingLog_Subscription] FOREIGN KEY ([SubscriptionId]) REFERENCES [dbo].[Subscription] ([SubscriptionId])
);

