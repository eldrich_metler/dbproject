﻿CREATE TABLE [dbo].[StoreSEO] (
    [StoreSEOId]  INT             IDENTITY (1, 1) NOT NULL,
    [StoreId]     INT             NULL,
    [Page]        NVARCHAR (1000) NULL,
    [Title]       NVARCHAR (1000) NULL,
    [Description] NVARCHAR (4000) NULL,
    [Keywords]    NVARCHAR (4000) NULL,
    [Status]      INT             NULL,
    [LastUpdated] DATETIME        NULL,
    [DateCreated] DATETIME        NULL,
    CONSTRAINT [FK_StoreSEO_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

