﻿CREATE TABLE [dbo].[ConfigPayGate] (
    [PayGateID]                NVARCHAR (50) NULL,
    [PayGatePassword]          NVARCHAR (50) NULL,
    [DateCreated]              DATETIME      NULL,
    [LastUpdated]              DATETIME      NULL,
    [ConfigPayGateId]          INT           NULL,
    [PaymentGatewayId]         INT           NULL,
    [Status]                   INT           NULL,
    [RecurringPayGateID]       NVARCHAR (50) NULL,
    [RecurringPayGatePassword] NVARCHAR (50) NULL
);

