﻿CREATE TABLE [dbo].[ProviderCoupon] (
    [ProviderCouponId] INT IDENTITY (1, 1) NOT NULL,
    [ProviderId]       INT NULL,
    [CouponId]         INT NULL,
    CONSTRAINT [PK_ProviderCoupon] PRIMARY KEY CLUSTERED ([ProviderCouponId] ASC),
    CONSTRAINT [FK_ProviderCoupon_Coupon] FOREIGN KEY ([CouponId]) REFERENCES [dbo].[Coupon] ([CouponId]),
    CONSTRAINT [FK_ProviderCoupon_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId])
);

