﻿CREATE TABLE [dbo].[Search] (
    [SearchId]    INT            IDENTITY (1, 1) NOT NULL,
    [StoreId]     INT            NULL,
    [CustomerId]  INT            NULL,
    [Phrase]      NVARCHAR (MAX) NULL,
    [Count]       INT            NULL,
    [DateCreated] DATETIME       NULL,
    [LastUpdated] DATETIME       NULL,
    CONSTRAINT [PK_Search] PRIMARY KEY CLUSTERED ([SearchId] ASC)
);

