﻿CREATE TABLE [dbo].[CompanyROEHistory] (
    [CompanyROEHistoryId] INT             IDENTITY (1, 1) NOT NULL,
    [CompanyROEId]        INT             NULL,
    [CompanyId]           INT             NULL,
    [CurrencyFromId]      INT             NULL,
    [CurrencyToId]        INT             NULL,
    [Rate]                DECIMAL (18, 6) NULL,
    [DateCreated]         DATETIME        NULL,
    [LastUpdated]         DATETIME        NOT NULL,
    CONSTRAINT [PK_CompanyROEHistory] PRIMARY KEY CLUSTERED ([CompanyROEHistoryId] ASC)
);

