﻿CREATE TABLE [dbo].[Tax] (
    [TaxId]            INT            IDENTITY (1, 1) NOT NULL,
    [TaxTypeId]        INT            NULL,
    [ConfigCurrencyId] INT            NULL,
    [Name]             NVARCHAR (50)  NULL,
    [Code]             NVARCHAR (50)  NULL,
    [PercentageRate]   DECIMAL (4, 2) NULL,
    [AmountRate]       DECIMAL (4, 2) NULL,
    [EffectiveDate]    DATETIME       NULL,
    [LastUpdated]      DATETIME       NULL,
    [DateCreated]      DATETIME       NULL,
    CONSTRAINT [PK_Tax] PRIMARY KEY CLUSTERED ([TaxId] ASC),
    CONSTRAINT [FK_Tax_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId]),
    CONSTRAINT [FK_Tax_TaxType] FOREIGN KEY ([TaxTypeId]) REFERENCES [dbo].[TaxType] ([TaxTypeId])
);

