﻿CREATE TABLE [dbo].[CompanyLanguageCode] (
    [CompanyLanguageCodeId] INT IDENTITY (1, 1) NOT NULL,
    [CompanyId]             INT NULL,
    [LanguageCodeId]        INT NULL,
    CONSTRAINT [FK_CompanyLanguageCode_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_CompanyLanguageCode_LanguageCode] FOREIGN KEY ([LanguageCodeId]) REFERENCES [dbo].[LanguageCode] ([LanguageCodeId])
);

