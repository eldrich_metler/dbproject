﻿CREATE TABLE [dbo].[MicrosoftSetting] (
    [MicrosoftSettingId] INT             IDENTITY (1, 1) NOT NULL,
    [MPNID]              NVARCHAR (20)   NULL,
    [StoreId]            INT             NULL,
    [LocationSetting]    INT             NULL,
    [Status]             INT             NULL,
    [DateCreated]        DATETIME        NULL,
    [LastUpdated]        DATETIME        NULL,
    [Margin]             DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_MicrosoftSetting] PRIMARY KEY CLUSTERED ([MicrosoftSettingId] ASC),
    CONSTRAINT [FK_MicrosoftSetting_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

