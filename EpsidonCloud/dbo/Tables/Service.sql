﻿CREATE TABLE [dbo].[Service] (
    [ServiceId]            INT             IDENTITY (1, 1) NOT NULL,
    [ProviderId]           INT             NULL,
    [ConfigServiceGroupId] INT             NULL,
    [Type]                 INT             NULL,
    [Name]                 NVARCHAR (250)  NULL,
    [Description]          NVARCHAR (MAX)  NULL,
    [Logo]                 NVARCHAR (50)   NULL,
    [MoreInformation]      NVARCHAR (MAX)  NULL,
    [FAQ]                  NVARCHAR (MAX)  NULL,
    [SLA]                  NVARCHAR (MAX)  NULL,
    [SystemRequirements]   NVARCHAR (MAX)  NULL,
    [APIIntegration]       INT             NULL,
    [Status]               INT             NULL,
    [LastUpdated]          DATETIME        NULL,
    [DateCreated]          DATETIME        NULL,
    [SEOTitle]             NVARCHAR (1000) NULL,
    [SEODescription]       NVARCHAR (4000) NULL,
    [SEOKeywords]          NVARCHAR (4000) NULL,
    CONSTRAINT [PK_Service] PRIMARY KEY CLUSTERED ([ServiceId] ASC),
    CONSTRAINT [FK_Service_ConfigServiceGroup] FOREIGN KEY ([ConfigServiceGroupId]) REFERENCES [dbo].[ConfigServiceGroup] ([ConfigServiceGroupId]),
    CONSTRAINT [FK_Service_Provider] FOREIGN KEY ([ProviderId]) REFERENCES [dbo].[Provider] ([ProviderId])
);



