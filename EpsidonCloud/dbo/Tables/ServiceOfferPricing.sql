﻿CREATE TABLE [dbo].[ServiceOfferPricing] (
    [ServiceOfferPricingId]    INT             IDENTITY (1, 1) NOT NULL,
    [ConfigServiceOfferTypeId] INT             NULL,
    [ServiceOfferId]           INT             NULL,
    [ConfigCurrencyId]         INT             NULL,
    [Amount]                   DECIMAL (18, 2) NULL,
    [Status]                   INT             NULL,
    [LastUpdated]              DATETIME        NULL,
    [DateCreated]              DATETIME        NULL,
    [VendorPrice]              DECIMAL (18, 2) NULL,
    [OfferPrice]               DECIMAL (18, 2) NULL,
    [OfferMargin]              DECIMAL (18, 3) NULL,
    [StoreId]                  INT             NULL,
    [PriceLockDuration]        INT             NULL,
    [PriceLockPrice]           DECIMAL (18, 2) NULL,
    [IsDisplay]                BIT             NULL,
    [CompanyId]                INT             NULL,
    CONSTRAINT [PK_ServiceOfferPricing] PRIMARY KEY CLUSTERED ([ServiceOfferPricingId] ASC),
    CONSTRAINT [FK_ServiceOfferPricing_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId]),
    CONSTRAINT [FK_ServiceOfferPricing_ConfigServiceOfferType] FOREIGN KEY ([ConfigServiceOfferTypeId]) REFERENCES [dbo].[ConfigServiceOfferType] ([ConfigServiceOfferTypeId]),
    CONSTRAINT [FK_ServiceOfferPricing_ServiceOffer] FOREIGN KEY ([ServiceOfferId]) REFERENCES [dbo].[ServiceOffer] ([ServiceOfferId])
);






GO
CREATE NONCLUSTERED INDEX [ncixServiceOfferPricingDisplay]
    ON [dbo].[ServiceOfferPricing]([Status] ASC, [StoreId] ASC, [IsDisplay] ASC)
    INCLUDE([ServiceOfferPricingId], [ServiceOfferId], [VendorPrice], [OfferPrice], [OfferMargin], [PriceLockDuration], [PriceLockPrice]);


GO
CREATE NONCLUSTERED INDEX [ncixServiceOfferPricing]
    ON [dbo].[ServiceOfferPricing]([ServiceOfferId] ASC, [Status] ASC, [StoreId] ASC, [CompanyId] ASC)
    INCLUDE([VendorPrice], [OfferPrice], [OfferMargin]);

