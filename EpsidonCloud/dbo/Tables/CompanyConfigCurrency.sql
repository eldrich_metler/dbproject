﻿CREATE TABLE [dbo].[CompanyConfigCurrency] (
    [CompanyConfigCurrencyId] INT IDENTITY (1, 1) NOT NULL,
    [CompanyId]               INT NULL,
    [ConfigCurrencyId]        INT NULL,
    CONSTRAINT [PK_CompanyConfigCurrency] PRIMARY KEY CLUSTERED ([CompanyConfigCurrencyId] ASC),
    CONSTRAINT [FK_CompanyConfigCurrency_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId]),
    CONSTRAINT [FK_CompanyConfigCurrency_ConfigCurrency] FOREIGN KEY ([ConfigCurrencyId]) REFERENCES [dbo].[ConfigCurrency] ([ConfigCurrencyId])
);

