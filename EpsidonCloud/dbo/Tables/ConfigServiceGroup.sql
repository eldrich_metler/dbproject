﻿CREATE TABLE [dbo].[ConfigServiceGroup] (
    [ConfigServiceGroupId] INT            IDENTITY (1, 1) NOT NULL,
    [CompanyId]            INT            NULL,
    [Name]                 NVARCHAR (100) NULL,
    [Status]               INT            NULL,
    [LastUpdated]          DATETIME       NULL,
    [DateCreated]          DATETIME       NULL,
    CONSTRAINT [PK_ConfigServiceGroup] PRIMARY KEY CLUSTERED ([ConfigServiceGroupId] ASC),
    CONSTRAINT [FK_ConfigServiceGroup_Company] FOREIGN KEY ([CompanyId]) REFERENCES [dbo].[Company] ([CompanyId])
);

