﻿CREATE TABLE [dbo].[Favourite] (
    [FavouriteId]   INT IDENTITY (1, 1) NOT NULL,
    [UserAccountId] INT NOT NULL,
    [ServiceId]     INT NOT NULL,
    CONSTRAINT [PK_Favourite] PRIMARY KEY CLUSTERED ([FavouriteId] ASC),
    CONSTRAINT [FK_Favourite_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId]),
    CONSTRAINT [FK_Favourite_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

