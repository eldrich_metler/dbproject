﻿CREATE TABLE [dbo].[MicrosoftCustomer] (
    [MicrosoftCustomerId]      INT            IDENTITY (1, 1) NOT NULL,
    [UserAccountId]            INT            NULL,
    [Domain]                   NVARCHAR (100) NULL,
    [Username]                 NVARCHAR (50)  NULL,
    [Password]                 NVARCHAR (500) NULL,
    [CustomerId]               NVARCHAR (50)  NULL,
    [CustomerAcceptance]       NVARCHAR (200) NULL,
    [Status]                   INT            NULL,
    [DateCreated]              DATETIME       NULL,
    [LastUpdated]              DATETIME       NULL,
    [Email]                    NVARCHAR (100) NULL,
    [CompanyName]              NVARCHAR (100) NULL,
    [IsBillingProfileComplete] BIT            NULL,
    [Qualification]            NVARCHAR (100) NULL,
    CONSTRAINT [PK_MicrosoftCustomer] PRIMARY KEY CLUSTERED ([MicrosoftCustomerId] ASC),
    CONSTRAINT [FK_MicrosoftCustomer_UserAccount] FOREIGN KEY ([UserAccountId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);





