﻿CREATE TABLE [dbo].[StoreTheme] (
    [StoreThemeId] INT            IDENTITY (1, 1) NOT NULL,
    [StoreId]      INT            NULL,
    [ThemeId]      INT            NULL,
    [Path]         VARCHAR (50)   NULL,
    [OutputFiles]  VARCHAR (5000) NULL,
    [MainCSS]      VARCHAR (50)   NULL,
    [LastUpdated]  DATETIME       NULL,
    [DateCreated]  DATETIME       NULL,
    CONSTRAINT [PK_StoreTheme_1] PRIMARY KEY CLUSTERED ([StoreThemeId] ASC),
    CONSTRAINT [FK_StoreTheme_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId]),
    CONSTRAINT [FK_StoreTheme_Theme] FOREIGN KEY ([ThemeId]) REFERENCES [dbo].[Theme] ([ThemeId])
);

