﻿CREATE TABLE [dbo].[AzureTermsAgreement] (
    [AzureTermsAgreementId]    INT           IDENTITY (1, 1) NOT NULL,
    [MicrosoftCustomerId]      INT           NOT NULL,
    [SignedByUserId]           INT           NOT NULL,
    [TermsAndConditionsAtTime] VARCHAR (MAX) NOT NULL,
    [DateCreated]              DATE          NOT NULL,
    [LastUpdated]              DATE          NOT NULL,
    PRIMARY KEY CLUSTERED ([AzureTermsAgreementId] ASC),
    FOREIGN KEY ([MicrosoftCustomerId]) REFERENCES [dbo].[MicrosoftCustomer] ([MicrosoftCustomerId]),
    FOREIGN KEY ([SignedByUserId]) REFERENCES [dbo].[UserAccount] ([UserAccountId])
);

