﻿CREATE TABLE [dbo].[CustomerCreditLimit] (
    [CustomerCreditLimitId] INT             NULL,
    [CreditLimit]           DECIMAL (18, 2) NULL,
    [CreditLimitUsed]       DECIMAL (18, 2) NULL,
    [CreditLimitType]       INT             NULL,
    [Status]                INT             NULL,
    [Reason]                VARCHAR (1000)  NULL,
    [ApprovedBy]            NVARCHAR (300)  NULL,
    [DateCreated]           DATETIME        NULL,
    [LastUpdated]           DATETIME        NULL
);

