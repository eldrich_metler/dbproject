﻿CREATE TABLE [dbo].[StoreCustomer] (
    [StoreCustomerId] INT IDENTITY (1, 1) NOT NULL,
    [StoreId]         INT NULL,
    [CustomerId]      INT NULL,
    CONSTRAINT [PK_StoreCustomer] PRIMARY KEY CLUSTERED ([StoreCustomerId] ASC),
    CONSTRAINT [FK_StoreCustomer_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_StoreCustomer_Store] FOREIGN KEY ([StoreId]) REFERENCES [dbo].[Store] ([StoreId])
);

