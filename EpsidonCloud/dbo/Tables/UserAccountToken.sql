﻿CREATE TABLE [dbo].[UserAccountToken] (
    [UserAccountTokenId] INT             IDENTITY (1, 1) NOT NULL,
    [Token]              NVARCHAR (1000) NULL,
    [UserAccountId]      INT             NULL,
    [StoreId]            INT             NULL,
    [ExpireDate]         DATETIME        NULL,
    [Type]               INT             NULL,
    [Status]             INT             NULL,
    [DateCreated]        DATETIME        NULL,
    [LastUpdated]        DATETIME        NULL,
    CONSTRAINT [PK_UserAccountToken] PRIMARY KEY CLUSTERED ([UserAccountTokenId] ASC)
);



