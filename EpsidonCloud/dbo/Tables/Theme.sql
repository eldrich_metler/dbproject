﻿CREATE TABLE [dbo].[Theme] (
    [ThemeId]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (50)  NULL,
    [Path]        VARCHAR (500) NULL,
    [InputFiles]  VARCHAR (MAX) NULL,
    [LastUpdated] DATETIME      NULL,
    [DateCreated] DATETIME      NULL,
    CONSTRAINT [PK_StoreTheme] PRIMARY KEY CLUSTERED ([ThemeId] ASC)
);

