﻿CREATE TYPE [dbo].[udtServiceTextLookupType] AS TABLE (
    [ServiceName] VARCHAR (128) NULL,
    [Reference]   VARCHAR (128) NULL);

