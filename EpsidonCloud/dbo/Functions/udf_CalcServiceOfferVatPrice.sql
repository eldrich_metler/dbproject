﻿CREATE FUNCTION [dbo].[udf_CalcServiceOfferVatPrice] 
(
	@ServiceOfferPricingId int,
	@IsSubCompanyService bit,	
	@SubCompanyResellerPrice decimal(18, 6),
	@SetOwnVendorPrice bit,
	@ProviderConfigCurrencyId int,
	@StoreTradeConfigCurrencyId int,
	@StoreConfigCurrencyId int,
	@CompanyROERate decimal(18, 6),
	@ParentCompanyROERate decimal(18, 6),
	@CompanyResellerPrice decimal(18, 6),
	@StoreROERate decimal(18, 6),
	@StoreOfferMargin decimal(18, 6),
	@StoreOfferPrice decimal(18, 6),
	@StoreVendorPrice decimal(18, 6),
	@StoreVAT decimal(18, 6),
	@StoreTaxType int,
	@PriceLockDuration int,
	@PriceLockPrice decimal(18, 6),
	@ServiceType int
)
RETURNS decimal(18, 2)
AS
BEGIN

	declare @StoreRetailPriceExclVAT decimal(22, 10)
	declare @StoreResellerPrice decimal(22, 10)
	declare @ActiveROERate decimal(22, 10)
	declare @ActiveROEPrice decimal(22, 10)
	declare @PriceBeforeStoreROE decimal(22, 10)
	declare @ServiceOfferPrice decimal(22, 10)
	declare @IsPriceLockActive bit = 0

	--if isnull(@StoreOfferMargin, 0) = 0 and isnull(@StoreOfferPrice, 0) = 0 and isnull(@PriceLockPrice, 0) = 0 
	--return 0

	if @IsSubCompanyService = 1
	begin
		set @StoreRetailPriceExclVAT = @SubCompanyResellerPrice
		set @StoreResellerPrice = @SubCompanyResellerPrice

		if @SetOwnVendorPrice = 1 
			set @ActiveROERate = @CompanyROERate
		else 
			set @ActiveROERate = @ParentCompanyROERate
	end
	else
	begin
		set @StoreRetailPriceExclVAT = @CompanyResellerPrice
		set @StoreResellerPrice = @CompanyResellerPrice
		set @ActiveROERate = @CompanyROERate
	end

	if @ProviderConfigCurrencyId = @StoreTradeConfigCurrencyId 
		set @ActiveROEPrice = @StoreRetailPriceExclVAT
	else if @CompanyROERate is not null and @CompanyROERate > 0
		set @ActiveROEPrice = @StoreRetailPriceExclVAT * @CompanyROERate
	
	if @ActiveROEPrice is not null and @ActiveROEPrice > 0 
	begin
		set @StoreRetailPriceExclVAT = @ActiveROEPrice
		set @StoreResellerPrice = @ActiveROEPrice		
	end

	if @ServiceOfferPricingId is not null and @ServiceOfferPricingId > 0
	begin
		if @IsSubCompanyService = 1
		begin
			set @StoreRetailPriceExclVAT = @SubCompanyResellerPrice
			set @StoreResellerPrice = @SubCompanyResellerPrice

			if @SetOwnVendorPrice = 1 
				set @ActiveROERate = @CompanyROERate
			else 
				set @ActiveROERate = @ParentCompanyROERate
		end
		else
		begin
			set @StoreRetailPriceExclVAT = @CompanyResellerPrice
			set @StoreResellerPrice = @CompanyResellerPrice
			set @ActiveROERate = @CompanyROERate
		end
		
		if @ProviderConfigCurrencyId = @StoreTradeConfigCurrencyId 
			set @ActiveROEPrice = @StoreRetailPriceExclVAT
		else if @CompanyROERate is not null and @CompanyROERate > 0
			set @ActiveROEPrice = @StoreRetailPriceExclVAT * @CompanyROERate	
			
		if @ActiveROEPrice is not null and @ActiveROEPrice > 0 
		begin
			set @StoreRetailPriceExclVAT = @ActiveROEPrice
			set @StoreResellerPrice = @ActiveROEPrice		
		end			
		
		if @StoreConfigCurrencyId <> @StoreTradeConfigCurrencyId 
		begin	
			set @PriceBeforeStoreROE = @StoreRetailPriceExclVAT

			if @StoreROERate is null set @ActiveROEPrice = @StoreRetailPriceExclVAT 
			else set @ActiveROEPrice = @StoreRetailPriceExclVAT * @StoreROERate

			set @StoreRetailPriceExclVAT = @ActiveROEPrice
			set @StoreResellerPrice = @ActiveROEPrice			
		end	
		
		-- Set Price Lock 
		if @PriceLockDuration > 0 and @PriceLockPrice > 0 set @IsPriceLockActive = 1 	
		
		-- Local Service						
		if @ServiceType = 1 set  @StoreRetailPriceExclVAT = @StoreVendorPrice

		if @IsPriceLockActive = 1
		begin
			set @StoreRetailPriceExclVAT = @PriceLockPrice
			set @StoreResellerPrice = @PriceLockPrice	
		end	
		else if @StoreOfferPrice is not null and @StoreOfferPrice <> 0
		begin
			set @StoreRetailPriceExclVAT = @StoreOfferPrice
		end				
		else if @StoreOfferMargin <> 0
		begin
			declare @StoreMarginPrice decimal(22, 10)
			declare @Percent decimal(22,10) = (@StoreOfferMargin/100)

			if @StoreOfferMargin is not null and @Percent < 1 
				set @StoreMarginPrice = (@StoreRetailPriceExclVAT / (1 - @Percent)) - @StoreRetailPriceExclVAT
			else if @StoreOfferMargin is not null and @Percent >= 1 
				set @StoreMarginPrice = @StoreRetailPriceExclVAT

			set @StoreRetailPriceExclVAT = @StoreRetailPriceExclVAT + @StoreMarginPrice
		end
	end

	declare @VATPrice decimal(22, 10) = 0

	if @StoreVAT is not null and @StoreVAT > 0 set @VATPrice = round(@StoreRetailPriceExclVAT, 2) * (@StoreVAT / 100)

	return @VATPrice
END