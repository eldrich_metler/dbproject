﻿

CREATE FUNCTION [dbo].[udf_CalcMarginValue]
(
	@Cost decimal(22,10),
	@Margin decimal(22,10)
)
RETURNS decimal(22,10)
AS
BEGIN
	declare @MarginPrice decimal(22, 10)
	declare @Percent decimal(22,10) = (@Margin/100)

	if @Margin is not null and @Percent < 1 
		set @MarginPrice = (@Cost / (1 - @Percent)) - @Cost
	else if @Margin is not null and @Percent >= 1 
		set @MarginPrice = @Cost	

	return @MarginPrice

END