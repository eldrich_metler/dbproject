﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace APublishDBFromDacPacTool
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> list = new List<string>();

            if (args.Length > 0)
            {
                list.AddRange(args);
            }

            DBPublisher.InstallDatabaseSchema();

            if (list.Contains("exitexe"))
            {                
                Console.WriteLine("Attempt has completed successfully.");
                Thread.Sleep(2000);
            }
            else
            {
                Console.WriteLine("Attempt has completed successfully. Press any key to continue....");
                Console.ReadKey();
            }
        }
    }
}
