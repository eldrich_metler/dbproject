﻿using Microsoft.SqlServer.Dac;
using System;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

namespace APublishDBFromDacPacTool
{
    public class DBPublisher
    {
        public static bool InstallDatabaseSchema()
        {
            try
            {
                Console.WriteLine("Begin InstallDatabaseSchema " + ConfigurationManager.ConnectionStrings["ADBToPublishToConnStr"].ConnectionString);
                InstallDatabaseSchema(ConfigurationManager.ConnectionStrings["ADBToPublishToConnStr"].ConnectionString, Directory.GetCurrentDirectory());
                Console.WriteLine("End InstallDatabaseSchema");
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR InstallDatabaseSchema : " + ex.ToString());
                return false;
            }

            return true;
        }
        private static void InstallDatabaseSchema(string connString, string folderPath)
        {
            string logMessage = "InstallDatabaseSchema {0} : {1}, {2}";

            try
            {
                string fileName = System.IO.Path.Combine(folderPath, ConfigurationManager.AppSettings["ADacPacToPublishFrom"].ToString());

                Console.WriteLine(string.Format(logMessage, "Start", fileName, System.Environment.NewLine));
                Console.WriteLine(string.Format(logMessage, "Conn", connString, System.Environment.NewLine));

                var conBuilder = new SqlConnectionStringBuilder(connString);              

                if (Directory.Exists(Path.Combine(folderPath, "Output")))
                    Directory.Delete(Path.Combine(folderPath, "Output"), true);

                Console.WriteLine(string.Format(logMessage, "Load DacPac", fileName, System.Environment.NewLine));

                using (var package = DacPackage.Load(fileName))
                {
                    var deployOptions = new DacDeployOptions()
                    {
                        BlockOnPossibleDataLoss = false,
                        ScriptDatabaseOptions = false,
                        AllowIncompatiblePlatform = true,                        
                    };

                    var pubOptions = new PublishOptions()
                    {
                        DeployOptions = deployOptions,                            
                        GenerateDeploymentReport = true,
                        GenerateDeploymentScript = true
                    };

                    var services = new DacServices(connString);
                    var pubResult = services.Script(package, conBuilder.InitialCatalog, pubOptions);                 

                    Directory.CreateDirectory(Path.Combine(folderPath, "Output"));
                    Console.WriteLine(string.Format(logMessage, "Publish result report", System.Environment.NewLine, pubResult.DeploymentReport));
                    File.WriteAllText(Path.Combine(folderPath, "Output", "DeploymentReport.xml"), pubResult.DeploymentReport);
                    Console.WriteLine(string.Format(logMessage, "Publish result db script", System.Environment.NewLine, pubResult.DatabaseScript));
                    File.WriteAllText(Path.Combine(folderPath, "Output", "DatabaseScript.sql"), pubResult.DatabaseScript);
                    Console.WriteLine(string.Format(logMessage, "Publish result master script", System.Environment.NewLine, pubResult.MasterDbScript));
                    File.WriteAllText(Path.Combine(folderPath, "Output", "MasterDbScript.sql"), pubResult.MasterDbScript);
                }

                Console.WriteLine(string.Format(logMessage, "Complete", fileName, System.Environment.NewLine));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
